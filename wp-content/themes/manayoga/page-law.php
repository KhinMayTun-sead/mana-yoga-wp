<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="law" class="subpage">

		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/law/law01.png" alt="特定商取引法に関して"></h2>

		    <section>
			    <table>
				    <tbody>
					    <tr>
						    <th>運営団体名</th>
						    <td>Mana Yoga</td>
					    </tr>
					    <tr>
						    <th>事業内容</th>
						    <td>ヨガスタジオ運営</td>
					    </tr>
					    <tr>
						    <th>運営責任者</th>
						    <td>高山 育美</td>
					    </tr>
					    <tr>
						    <th>設立年月</th>
						    <td>2015年05月04日</td>
					    </tr>
					    <tr>
						    <th>資本金</th>
						    <td>300万円</td>
					    </tr>
					    <tr>
						    <th>本社</th>
						    <td>〒150-0021　東京都渋谷区恵比寿西1-2-1 　エビスマンション706</td>
					    </tr>
					    <tr>
						    <th>事務所</th>
						    <td>〒150-0021　東京都渋谷区恵比寿西1-2-1 　エビスマンション706</td>
					    </tr>
					    <tr>
						    <th>問い合わせ先</th>
						    <td>
							    <ul>
								    <li><span>電話番号</span><p>03-6455-1756</p></li>
								    <li><span>メールアドレス</span><p><a href="mailto:info@manayoga.jp">info@manayoga.jp</a></p></li>
						    </td>
					    </tr>
					    <tr>
						    <th>営業時間</th>
						    <td>7:00～22:30　お問い合わせは24時間受け付けております。</td>
					    </tr>
					    <tr>
						    <th>定休日</th>
						    <td>年中無休（休業：年末年始・臨時）</td>
					    </tr>
					    <tr>
						    <th>販売価格</th>
						    <td><a href="price.html">料金システム</a>をご確認ください。</td>
					    </tr>
					    <tr>
						    <th>商品代金以外の必要料金</th>
						    <td>消費税</td>
					    </tr>
					    <tr>
						    <th>商品代金のお支払方法</th>
						    <td>
							    【クレジットカード】取り扱い可能ブランド　VISA、MASTER、JCB<br />
								ご注文手続き完了と同時に決済されます。<br />
								決済情報はSSLで暗号化され、安全性を確保しております。<br />
								またCloud Payment社のSSL証明書はベリサインにて発行されております。<br />
								<a href="https://www.cloudpayment.co.jp/" target="_blank">https://www.cloudpayment.co.jp/</a>
						    </td>
					    </tr>
					    <tr>
						    <th>商品のお渡し時期</th>
						    <td>ご入金確認後、カードを発行いたします。</td>
					    </tr>
					    <tr>
						    <th>コースの変更</th>
						    <td>15日前までに申し出</td>
					    </tr>
					    <tr>
						    <th>マンスリー会員停止</th>
						    <td>マンスリー会員の停止を希望される場合は、停止希望月の前々月の決済予定日の前日までにお申し出いただく必要がございます。</td>
					    </tr>
					</tbody>
				</table>
			    
		    </section>



	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>