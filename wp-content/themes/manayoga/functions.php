<?php 
function catbase_function($link) {
    return str_replace("/category/", "/", $link);
}
function catbase_flush_rules() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
function catbase_rewrite($wp_rewrite) {
    $new_rules = array('(.+)/page/(.+)/?' => 'index.php?category_name='.$wp_rewrite->preg_index(1).'&paged='.$wp_rewrite->preg_index(2));
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}
add_filter('user_trailingslashit', 'catbase_function');
add_action('init', 'catbase_flush_rules');
add_filter('generate_rewrite_rules', 'catbase_rewrite');

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

?>