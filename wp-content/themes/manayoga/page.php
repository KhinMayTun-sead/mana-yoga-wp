<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="instructor" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01.png">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor02.png" alt="インストラクター紹介"></h2>

<?php if (have_posts()) : // WordPress ループ
while (have_posts()) : the_post(); // 繰り返し処理開始 ?>

<?php the_content(); ?>

<?php endwhile; // 繰り返し処理終了
else : // ここから記事が見つからなかった場合の処理 ?>
<div class="post">
<h2>記事はありません</h2>
<p>お探しの記事は見つかりませんでした。</p>
</div>
<?php endif; ?>

		    <section id="listreturn" class="ta_right">
				<a href="<?php echo home_url('/instructor/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor06.png"></a>

		    </section>




	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>