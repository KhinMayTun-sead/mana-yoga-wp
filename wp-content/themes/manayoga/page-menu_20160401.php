<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="menu" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/menu/menu01.png" alt="マナヨガ">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/menu/menu02.png" alt="レッスンメニュー"></h2>

		    <section id="context">
				<p>
マナヨガでは、初心者の方から本格的にヨガに取り組んでいる方まで<br />
楽しんで満足していただけるよう幅広いクラスを用意しています。<br />
リラクゼーション効果の高いゆったりクラス、気持ちよく汗をかくパワー系クラス、<br />
さらにローラーピラティスやMen'sクラスなどのスペシャルクラスもあります。<br />
ご自身の体調やその日の気分、その時の目的に合わせて自由にクラスをお選びいただけます。
				</p>
		    </section>

		    <section class="menu menu01">
				<dl>
					<dt>ビギナー</dt>
					<dd>「ヨガは初めて！」という方、大歓迎！<br />
基礎から学べる、入門・初級クラスです。</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01">
				<dl>
					<dt>優しいヨガ</dt>
					<dd>
優しい気持ちと優しいポーズで<br />
日頃疲れた身体を労わってあげましょう。<br />
ひとつひとつのポーズをゆっくり丁寧にお伝えしていきます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01">
				<dl>
					<dt>ハタ＆リラックスヨガ</dt>
					<dd>
ゆっくりと行う動きとリラックスポーズで、<br />
身体と心を整えていきます。<br />
ヨガ初心者や身体の硬さが不安という方にもオススメです。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01">
				<dl>
					<dt>お疲れリリースヨガ</dt>
					<dd>
1週間の疲れを和らげるための、ゆったりとしたヨガクラスです。<br />
心地よく身体を伸ばしたり捩じったりしながら、<br />
血流やリンパを刺激して疲労物質を流していきます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01h h">
				<dl>
					<dt>月のヨガ</dt>
					<dd>
ヨガの呼吸やポーズを意識して行い、<br />
その違いがあるかないかをじっくり見ていくクラスです。<br />
（動きは少なめ）<br />
身体や心で感じた違いに気づくことによって<br />
普段の暮らしの中で肩の力を抜いて生きて行く<br />
ちょっとしたコツがわかってきたら<br />
少しだけいつもより楽になれるかも…。<br />
<br />
「お休みなんだっけ？」という<br />
頑張り屋さんのためのゆったりヨガです。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01h h">
				<dl>
					<dt>ヨガニードラ</dt>
					<dd>
ヨガニードラの「ニードラ」とは「眠り」という意味で、<br />
シャバーサナ（休息のポーズ）の状態で行うヨガになります。<br />
インストラクターの誘導を聞きながら、<br />
心身を深くリラックスさせていきます。<br />
また、まどろんだ半覚醒の状態で「サンカルパ（決意）」を<br />
思い浮かべることで、潜在意識に働きかけ自己実現力を高めます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>ベーシック</dt>
					<dd>ヨガの基本のアーサナ（ポーズ）を中心としたクラス。<br />
オールレベル対応です。</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>元気アロマヨガ</dt>
					<dd>
柑橘系のアロマの香りの中、呼吸を深め、<br />
全身をほぐし、ひきしめ、休めるクラス。<br />
太陽礼拝あり。ほんのり汗をかいて、エネルギーチャージされます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>姿勢調整ヨガ</dt>
					<dd>
サポートの物を使いながら、<br />
ポーズ一つ一つを丁寧に行い、姿勢を整えるヨガです。<br />
初心者の方からご参加いただけます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>お腹スッキリヨガ</dt>
					<dd>
体幹を強くしてポッコリお腹を解消、<br />
内臓にも働きかけてデトックス効果も期待できます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>自分コントロール</dt>
					<dd>
このクラスではヨガの呼吸法とポーズ、瞑想を行い、<br />
ココロとカラダの両方からアプローチをしていきます。<br />
※動きの時間は若干少なめですが、<br />
　強弱はお好みでアレンジ可能です。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02w">
				<dl>
					<dt>女子力UPヨガ</dt>
					<dd>
日頃のコリをほぐし→内側の筋肉を締める→穏やかに<br />
リラックスするという3ステップ。<br />
憧れるような引き締まったBODY！<br />
そして女性らしいしなやかな心とカラダを作っていきます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02wh h">
				<dl>
					<dt>子宮美人ヨガ</dt>
					<dd>
現代生活においてちぐはぐになってしまっている、<br />
身体の緩める・締める、のバランスを整えていくクラスです。<br />
身体の軸を意識して楽に立てる姿勢をご紹介していきます。<br />
生理痛・PMS・尿漏れなど女性特有の悩みにアプローチしていきます。<br />
<br />
女性のための身体づくりはじまめしょう♪
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02h h">
				<dl>
					<dt>ファンクショナル<br />
ローラーピラティス</dt>
					<dd>
快適な歩行へ導くピラティス。<br />
不安定な形状のローラーを使用し、日常生活の癖などで歪んだカラダを整えて歩き易いカラダへ！！<br />
<div class="left">
	<img src="<?php echo get_template_directory_uri(); ?>/img/menu/menu09.png" alt="ファンクショナルローラーピラティス">
</div>
<div class="right">

</div>
<div class="clearfix"></div>

					</dd>
				</dl>
				<div class="clearfix"></div>
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>美脚ヨガ</dt>
					<dd>
しなやかな筋肉のスッキリ美脚を目指しましょう！<br /> 
むくみ、老廃物の滞りをリセットして、循環力も高めます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>フロー</dt>
					<dd>
呼吸のリズムに合わせ、アーサナ（ポーズ）をつないで<br />
流れるように行います。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02m">
				<dl>
					<dt>For men</dt>
					<dd>
ヨガに興味はあるけど...<br />
そんな男性のための男性限定クラスです。<br />
しっかり動いて締まった身体を目指します。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu03">
				<dl>
					<dt>カラダメンテナンス</dt>
					<dd>
身体の深部の筋肉（インナーマッスル）に働きかけ、<br />
体幹（コア）を鍛えていきます。<br />
<br />
代謝UPで身体を締めよう！
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu03">
				<dl>
					<dt>アナトミック骨盤ヨガ</dt>
					<dd>
キツイ！でも効く！！<br />
ゆっくりとした動きでジックリ筋肉を動かして骨盤周辺、<br />
股関節周辺の筋肉を活性化させて柔軟性と筋力を養います！<br />
体型を整え、引き締める、自力整体のようなヨガを目指しています。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu03">
				<dl>
					<dt>パワー</dt>
					<dd>
運動量が多く、ダイナミック、ハードな動きを<br />
呼吸に合わせて行います。<br />
「身体を動かしたい！」という方にはピッタリです。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu02">
				<dl>
					<dt>朝ヨガ</dt>
					<dd>
ヨガで朝活しませんか？<br />
45分のレッスンでスッキリ目覚めて1日を充実させましょう。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01h h">
				<dl>
					<dt>笑いヨガ</dt>
					<dd>
「笑いヨガ」は誰にでもできる笑いの健康体操です。<br />
ユーモア・冗談・コメディは使いません。笑いの体操とヨガの<br />
呼吸法を組み合わせて行います。何も心配ありません。<br />
笑うことで、笑いは伝染していき、無理なく笑うことができます。<br />
腹筋・横隔膜・表情筋など普段意識しない筋肉を自然に使い<br />
身体の未知なる部分が目覚めるのを実感することができます。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>

		    <section class="menu menu01">
				<dl>
					<dt>シニアヨガ</dt>
					<dd>
65歳以上のかた限定のクラス。<br />
椅子やブロックなどを使用し、無理なくクラスを行います。
					</dd>
				</dl>
				<div class="clearfix"></div>
				
		    </section>


	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>