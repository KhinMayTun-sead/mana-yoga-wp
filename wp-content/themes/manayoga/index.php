<?php get_header(); ?>
		        
		<div id="main">
	        <div id="silideBox">
	            <div>
	                <ul class="rslides" id="slider1">
	                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/top/mana01.png" alt="マナヨガ" width="1000" height="410"></li>
	                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/top/mana02.png" alt="マナヨガ" width="1000" height="410"></li>
	                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/top/mana03.png" alt="マナヨガ" width="1000" height="410"></li>
	                    <li><img src="<?php echo get_template_directory_uri(); ?>/img/top/mana04.png" alt="マナヨガ" width="1000" height="410"></li>
		            </ul>
	            </div>
	        </div><!-- silideBox -->

	        <div class="main_v2">
	        	マナヨガは恵比寿にある少人数制のヨガスタジオです。<br>
	        	リラックスやダイエットなど、ひとりひとりの目的に合わせてレッスンを行なっています。<br>
	        	体が硬い、運動が苦手という方もご安心ください。ティーチャートレーニングを修了した<br>
	        	インストラクターが、しっかり向き合います。<br>
	        	自分自身のカラダと心と対話しながら、私たちと一緒にヨガで心地よいひとときを過ごしませんか。
	        </div>
	        <div class="clearfix"></div>
		</div>

	<div class="wrap">

	    <article id="top">

		    <section id="info">
			    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/top/info.png"></h2>
	
	            	<?php
					$category = get_the_category();
					$cat_id   = $category[0]->cat_ID;
					$cat_name = $category[0]->cat_name;
					$cat_slug = $category[0]->category_nicename;
					$newslist = get_posts( array(
					'posts_per_page' => 1 ,//取得記事件数
					'category_name'       => 'information'
					));
					foreach( $newslist as $post ):
					setup_postdata( $post );
					?>
                    	<?php the_content(); ?>
	                <?php
					endforeach;
					wp_reset_postdata();
					?>

		    </section>

		    <section id="blog">
			    <h2><p class="ta_right"><a href="<?php echo home_url('/blog/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/top/bloglist.png"></a></p></h2>
				<ul>
	            	<?php
					$newslist = get_posts( array(
					'posts_per_page' => 5, //取得記事件数
					'cat'            =>-2
					));
					foreach( $newslist as $post ):
					setup_postdata( $post );
					?>

					<li><a href="<?php the_permalink(); ?>"><date><?php echo get_the_date('Y年m月d日') ?></date><span><?php the_title(); ?></span></a></li>
					<!--<li><date>2015年07月20日</date><span>いよいよ夏目前！ 薄着になる季節がやってきました！</span></li>
					<li><date>2015年07月20日</date><span>いよいよ夏目前！ 薄着になる季節がやってきました！</span></li>
					<li><date>2015年07月20日</date><span>いよいよ夏目前！ 薄着になる季節がやってきました！</span></li>
					<li><date>2015年07月20日</date><span>いよいよ夏目前！ 薄着になる季節がやってきました！</span></li>-->
	                <?php
					endforeach;
					wp_reset_postdata();
					?>

				</ul>
		    </section>

		    <section id="first">
			    <p>マナヨガへようこそ！ <br>マナヨガは恵比寿にある少人数制のヨガスタジオです。資格を持っているインストラクターが丁寧にサポートします。はじめての方も経験者の方も、お気軽に体験レッスンへお越しください。</p>	
		    </section>

		    <section id="characteristic">
				<h2><img src="<?php echo get_template_directory_uri(); ?>/img/top/top01.png" alt="マナヨガの特徴"></h2>
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/top/top02.png" alt="インストラクターは全員有資格者"></h3>
				<p class="context">
インストラクター全員が、ヨガをきちんと学んだ証である全米ヨガアライアンスの資格“RYT200”を保持しています。<br />
さらにそれぞれが異なったバックグラウンドを持つ個性あふれる私たちが<br />
みなさんのヨガライフをステキに彩るお手伝いをいたします。
				</p>
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/top/top03.png" alt="セミプライベート制でみなさんのことをしっかりとケアします"></h3>
				<p class="context">
みなさんのことをすぐに覚えられるように、少しの変化にも気づけるように、<br />
みなさんの声に耳を傾けられるように…<br />
私たちの「少しでも寄り添ってヨガをお伝えできたら」という気持ちから、クラスは最大8名の少人数にしました。
				</p>
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/top/top04.png" alt="カルテ制度で「安全・安心」"></h3>
				<p class="context">
インストラクター全員が、みなさんの体調や近況など、把握しておくべきことを共有するために<br />
おひとりずつのカルテを作成します。<br />
みなさんに安心してレッスンを受けていただけるよう、細心の注意を払い、体の安全をサポートします。
				</p>

		    </section>




	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>