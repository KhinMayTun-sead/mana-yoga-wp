<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="faq" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq01.png">
		    </section>

		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq02.png" alt="よくあるご質問"></h2>


		    <section>
			    <h3><img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq03.png" alt="体験レッスンについて"></h3>

			    <dl>
				    <dt>体験レッスンに申し込むにはどうすればいいですか？</dt>
				    <dd><a href="<?php echo home_url('/schedule/'); ?>">スケジュール</a>を確認し、お好きなクラスを専用の予約システムより<br />ご予約ください。<br />詳しくはこちらのページもご覧ください。<br /><span><a href="<?php echo home_url('/register/'); ?>">→体験レッスンについて</a></span></dd>
			    </dl>			    

			    <dl>
				    <dt>ウェアはレンタルできますか？</dt>
				    <dd>はい。マナヨガではレンタルウェアを無料でご用意しております。</dd>
			    </dl>			    

			    <dl>
				    <dt>着替えは必要ですか？</dt>
				    <dd>動きやすい服装に着替えてください。更衣室・ロッカーもご用意しております。またレンタルウェアを無料でご用意しております。</dd>
			    </dl>			    

			    <dl>
				    <dt>体験レッスンは何回でも受けられますか？</dt>
				    <dd>最大3回まで体験レッスンを受けていただけるチケットを販売しています。ご都合に合わせてお求めください。</dd>
			    </dl>			    

			    <dl>
				    <dt>服装はどんな格好がいいですか？</dt>
				    <dd>ジャージやTシャツなど動きやすい格好でお願いします。体験レッスンは裸足で行いますので、ヨガ用の靴や靴下は不要です。</dd>
			    </dl>			    

			    <dl>
				    <dt>レッスン前に食事をしても大丈夫？</dt>
				    <dd>軽く食べる程度でしたら問題ありません。運動の多いクラスもありますので、気分が悪くならないようご調節ください。</dd>
			    </dl>			    

		    </section>
		    
		    <section>
			    <h3><img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq06.png" alt="予約・キャンセルについて"></h3>
			    
			    <dl>
				    <dt>予約はどのように取ればいいのですか？</dt>
				    <dd>スケジュールをご確認いただき、専用の予約フォームからご予約ください。<br /><span><a href="<?php echo home_url('/schedule/'); ?>">→スケジュールへ</a></span></dd>
			    </dl>			    

			    <dl>
				    <dt>予約をしなくてもレッスンが受けられますか？</dt>
				    <dd>事前予約が必要となります。レッスン開始予定の3時間前までに専用の予約システムよりご予約ください。</dd>
			    </dl>			    
			    
			    <dl>
				    <dt>キャンセルしたいのですが、どのようにすればいいですか？</dt>
				    <dd>あらかじめお電話またはメールにてご連絡ください。プライベートレッスン・出張レッスンはキャンセル料が発生いたします。<br /><span><a href="<?php echo home_url('/price/'); ?>">→料金のご案内を見る</a></span></dd>
			    </dl>			    

		    </section>

		    <section>
			    <h3><img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq07.png" alt="料金について"></h3>
			    
			    <dl>
				    <dt>マンスリー会員でも笑いヨガを受けられますか？</dt>
				    <dd>はい、受けられます。</dd>
			    </dl>			    

			    <dl>
				    <dt>マンスリー2・4・8回の会員です。今月もう1回行きたいのですが・・・</dt>
				    <dd>1回あたり2,500円で追加レッスンをお受けいただけます。</dd>
			    </dl>			    
			    
			    <dl>
				    <dt>マンスリー8回の会員です。今月6回しか行けなかったのですが・・・</dt>
				    <dd>月内に使用できなかったレッスンの1回分は翌月に持ち越すことは可能です。レッスン回数の変更などインストラクターにご相談ください。</dd>
			    </dl>			    

			    <dl>
				    <dt>私のカードで友だちもレッスンを受けることができますか？</dt>
				    <dd>できません。まずは体験レッスンへご参加いただくか、ご入会ください。<br /><span><a href="<?php echo home_url('/register/'); ?>">→体験レッスンについて</a></span></dd>
			    </dl>			    
			    
			    <dl>
				    <dt>マンスリー会員になるのはなにが必要ですか？</dt>
				    <dd>クレジットカードが必要です。</dd>
			    </dl>			    

			    <dl>
				    <dt>マンスリー会員を解約するにはどうすればいいですか？</dt>
				    <dd>退会希望月の前々月の決済予定日前日までにお申し出ください。</dd>
			    </dl>			    

		    </section>

		    <section>
			    <h3><img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq08.png" alt="スタジオの利用について"></h3>
			    
			    <dl>
				    <dt>定休日はありますか？</dt>
				    <dd>定休日はありません。年末年始やお盆の時期にお休みをいただいています。その他、インストラクターの研修などで臨時でお休みをいただく場合は、事前に告知いたします。</dd>
			    </dl>			    

			    <dl>
				    <dt>会員カードを失くしてしまいました・・・</dt>
				    <dd>インストラクターまでご連絡ください。カード再発行の手続きを行います。</dd>
			    </dl>			    
			    
			    <dl>
				    <dt>トイレや更衣室はありますか？</dt>
				    <dd>ご用意しております。</dd>
			    </dl>			    

			    <dl>
				    <dt>駐車場はありますか？</dt>
				    <dd>近隣にコインパーキングがございます。そちらをご利用ください。</dd>
			    </dl>			    
			    
			    <dl>
				    <dt>ビルのエントランスが開かないのですが・・・</dt>
				    <dd>20：00以降エントランスが施錠されます。遅い時間のレッスンにご予約された場合は、事前にパスワードをお渡しいたします。</dd>
			    </dl>			    
		    

		    </section>


	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>