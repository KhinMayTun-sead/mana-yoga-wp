<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="blog" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog01.png" alt="manayoga blog">
		    </section>

		    <section>
                 <?php if (have_posts()) : 
				while (have_posts()) : the_post(); ?>
                <div class="entry" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <div class="content">
                    <p class="data"><?php echo get_the_date('Y年m月d日') ?></p>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div id="mainContent">
                        <?php the_content() ?>
                    </div>
                </div>

				<div class="navigation">
					<?php if( get_previous_post() ): ?>
					<div class="left"><?php previous_post_link('%link', '<img src="' .get_template_directory_uri(). '/img/blog/blog03.png" alt="前の記事">'); ?></div>
					<?php endif;
					if( get_next_post() ): ?>
					<div class="right"><?php next_post_link('%link', '<img src="' .get_template_directory_uri(). '/img/blog/blog04.png" alt="次の記事">'); ?></div>
					<?php endif; ?>
					<div class="clearfix"></div>
				</div>

			   <?php endwhile; // 繰り返し処理終了
				else : // ここから記事が見つからなかった場合の処理 ?>
					<h2>記事はありません</h2>
					<p>お探しの記事は見つかりませんでした。</p>
				<?php endif; ?>
			</section>

			<section>
				<div class="ta_right"><a href="<?php echo home_url('/blog/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog02.png" alt="一覧を見る"></a></div>
			</section>

		    <section>
			    <h2><p>新着記事</p></h2>
				<ul>
	            	<?php
					$newslist = get_posts( array(
					'posts_per_page' => 5, //取得記事件数
					'cat'            =>-2
					));
					foreach( $newslist as $post ):
					setup_postdata( $post );
					?>

					<li><a href="<?php the_permalink(); ?>"><date><?php echo get_the_date('Y年m月d日') ?></date><span><?php the_title(); ?></span></a></li>

	                <?php
					endforeach;
					wp_reset_postdata();
					?>

				</ul>

			    
		    </section>


	    </article>
	
<?php get_sidebar(2); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>