<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="photogallery" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery01.png">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery02.png" alt="フォトギャラリー"></h2>

		    <section id="image">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery03.png" alt="photogallery03">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery04.png" alt="photogallery04">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery05.png" alt="photogallery05">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery06.png" alt="photogallery06">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery07.png" alt="photogallery07">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery08.png" alt="photogallery08">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery09.png" alt="photogallery09">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery10.png" alt="photogallery10">
				<img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery11.png" alt="photogallery11" id="aumnie">
		    </section>




	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>