<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="access" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/access/access01.png">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/access/access02.png" alt="スタジオへのアクセス"></h2>

		    <section id="map">
			    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1621.0789035824832!2d139.70764785591174!3d35.64848343937602!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b4176d91c3f%3A0xbb4d894d54ad8bb6!2z44Oe44OK44Oo44Ks44K544OA44K444Kq!5e0!3m2!1sja!2sjp!4v1452220145330" width="614" height="440" frameborder="0" style="border:0" allowfullscreen></iframe>
		    </section>

		    <section id="info">
				<div class="left">
					<img src="<?php echo get_template_directory_uri(); ?>/img/access/access04.png" alt="map">
				</div>
				<div class="right">
				    <h3>マナヨガ</h3>
				    <dl>
					    <dt>住　　所</dt>
					    <dd>〒150-0021<br />東京都渋谷区恵比寿西1-2-1<br />エビスマンション706</dd>
					    <dt>電話番号</dt>
					    <dd>03-6455-1756</dd>
					    <dt>営業時間</dt>
					    <dd>7：00～22：30</dd>
				    </dl>
				    <div class="clearfix"></div>
				    <p>
					    ※休館日はお問い合わせください。<br />
						※セキュリティの関係上、当ビルは20：00以降入口が施錠されます。<br />
						　解錠方法はお問い合わせください。<br />
						※レッスン中は電話に出られない場合もございますので<br />
						　ご容赦ください。
				    </p>
				</div>
				<div class="clearfix"></div>

		    </section>


		    <section id="street">
				<div class="street1">
					<p>
恵比寿西口のエビスさん横、交番前の信号を渡る。<br />
駒沢通りと垂直に走るソフトバンク（向かって右手）と<br />
野郎ラーメン（向かって左手）がある道をそのまま直進。
					</p>
				</div>
				<div class="street2">
					<p>
20ｍほど進むと右手に韓国料理ひまわりがあります。<br />
そこの２差路を右に曲がり、線路側に進んでください。<br />
線路に沿って30ｍほど直進すると<br />
左手にエビスマンションの入り口がございます。
					</p>
				</div>
				<div class="street3">
					<p>
※ココカラファインのビルの7Fですが、<br />
線路側に回らないとビルの入り口が<br />
ございませんのでご注意くださいませ。
					</p>
				</div>

		    </section>



	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>