	<div id="footer_zero">
		<img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor_line1.png">
		<div class="autoplay">
			<div><a href="<?php echo home_url('/instructor/instructor01/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor01.png"></a></div>
<!--			<div><a href="<?php echo home_url('/instructor/instructor02/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor02.png"></a></div>-->
			<div><a href="<?php echo home_url('/instructor/instructor03/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor03.png"></a></div>
<!--			<div><a href="<?php echo home_url('/instructor/instructor04/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor04.png"></a></div>-->
			<!--<div><a href="<?php echo home_url('/instructor/instructor05/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor05.png"></a></div>-->
			<div><a href="<?php echo home_url('/instructor/instructor06/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor06.png"></a></div>
			<div><a href="<?php echo home_url('/instructor/instructor07/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor07.png"></a></div>
<!--			<div><a href="<?php echo home_url('/instructor/instructor08/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor08.png"></a></div>-->
<!--			<div><a href="<?php echo home_url('/instructor/instructor09/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor09.png"></a></div>-->
<!--			<div><a href="<?php echo home_url('/instructor/instructor10/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor10.png"></a></div>-->
			<div><a href="<?php echo home_url('/instructor/instructor11/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor11.png"></a></div>
<!--			<div><a href="<?php echo home_url('/instructor/instructor12/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor12.png"></a></div>-->
<!--			<div><a href="<?php echo home_url('/instructor/instructor14/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor14.png"></a></div>-->
			<div><a href="<?php echo home_url('/instructor/instructor15/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor15.png"></a></div>
<!--			<div><a href="<?php echo home_url('/instructor/instructor16/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor16.png"></a></div>-->
			<div><a href="<?php echo home_url('/instructor/instructor17/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor17.png"></a></div>
			<!--<div><a href="<?php echo home_url('/instructor/instructor18/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor18.png"></a></div>-->
<!--			<div><a href="<?php echo home_url('/instructor/instructor19/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor19.png"></a></div>-->
<!--			<div><a href="<?php echo home_url('/instructor/instructor21/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor21.png"></a></div>-->
            <div><a href="<?php echo home_url('/instructor/instructor23/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor23.png"></a></div>
		</div>
		<img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor_line2.png">

		<div class="trial_w">
			<div><a href="<?php echo home_url('/register/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/trial_w.png"></a></div>
		</div>
		<div class="ta_right" id="return"><a id="return-page-top"><img src="<?php echo get_template_directory_uri(); ?>/img/common/return.png"></a></div>
		<div class="clearfix"></div>
	</div>


    <footer id="footer">
	    <div class="inner">
		    <div class="logo">
		    	<a href="./"><img src="<?php echo get_template_directory_uri(); ?>/img/common/footerlogo.png"></a>
		    </div>
		    <div id="link1">
				<ul>
					<li><a href="<?php echo home_url('/schedule/'); ?>">スケジュール</a></li>
					<li><a href="<?php echo home_url('/menu/'); ?>">レッスンメニュー</a></li>
					<li><a href="<?php echo home_url('/price/'); ?>">料金システム</a></li>
					<li><a href="<?php echo home_url('/instructor/'); ?>">インストラクター紹介</a></li>
					<li><a href="<?php echo home_url('/register/'); ?>">ご入会の流れ</a></li>
					<li><a href="<?php echo home_url('/access/'); ?>">アクセス</a></li>
				</ul>
		    </div>

		    <div id="link2">
				<ul>
					<li><a href="<?php echo home_url('/register/'); ?>">体験レッスンを予約する</a></li>
					<li><a href="<?php echo home_url('/faq/'); ?>">よくある質問</a></li>
					<li><a href="<?php echo home_url('/photogallery/'); ?>">フォトギャラリー</a></li>
					<li><a href="<?php echo home_url('/blog/'); ?>">manayoga blog</a></li>
					<li><a href="<?php echo home_url('/law/'); ?>">特定商取引法に基づく表記</a></li>
					<li><a href="<?php echo home_url('/policy/'); ?>">プライバシーポリシー</a></li>
				</ul>
		    </div>
			<div class="clearfix"></div>

	        <div id="copy">
		        <img src="<?php echo get_template_directory_uri(); ?>/img/common/footerbg.png">
		        <p>Copyright &copy; 2015 MANAYoga All Rights Reserved.</p>
	        </div>
        </div>

    </footer>


    <script src="<?php echo get_template_directory_uri(); ?>/js/common.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/responsiveslides.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/slick.min.js" type="text/javascript"></script>

    <script type="text/javascript">
//<![CDATA[
    // You can also use "$(window).load(function() {"
    $(function () {

      $("#slider1").responsiveSlides({
        auto: true,
        pager: true,
        nav: false,
        speed: 5000,
        timeout: 5500,
        namespace: "callbacks",
        before: function () {
          $('.events').append("<li>before event fired.<\/li>");
        },
        after: function () {
          $('.events').append("<li>after event fired.<\/li>");
        }
      });


    });

    //]]>
    </script><!-- end -->

    <script type="text/javascript">
		$('.autoplay').slick({
		  slidesToShow: 8,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 5000,
		  draggable: false,
		});
    </script><!-- end -->


    <script type="text/javascript">
//<![CDATA[
    // You can also use "$(window).load(function() {"
    $(function () {
	$("#return-page-top").click(
		function(){
			//[id:return-page-top]をクリックしたら起こる処理
			$("html,body").animate({scrollTop:0},"slow");
		}
	);

    });

    //]]>
    </script><!-- end -->

<?php wp_footer(); ?>

</body>
</html>
