<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="price" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/price/price01.png">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/price/price02.png" alt="料金システム"><span>※料金はすべて税込価格です。</span></h2>
		    <section id="context">
			    <p>ひと月の内に何回でも好きなだけレッスンに参加できる「フリーパス」や<br />
				    ご利用頻度に合わせてご購入いただける「回数チケット」をご用意しております。<br />
				    ご自身のライフスタイルに合わせて、お選びください。</p>
				<div><img src="<?php echo get_template_directory_uri(); ?>/img/price/price03.png"></div>

		    </section>

		    <section id="monthly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/price/price04.png" alt="マンスリー会員"></h3>
				<p class="context">じっくり続けてレッスンに通いたいという方へおすすめのおトクな月謝制です。<br />毎月、設定した回数内でお好きな時に受講していただけます。</p>
				<table>
					<tbody>
						<tr>
							<th class="times">&nbsp;</th>
							<th class="price">料金</th>
							<th class="limit">有効期限</th>
						</tr>
						<tr>
							<td class="times">フリーパス</td>
							<td class="price">25,000円</td>
							<td class="limit" rowspan="4">1ヶ月</td>
						</tr>
						<tr>
							<td class="times">8回</td>
							<td class="price">18,000円（2,250円／回）</td>
						</tr>
						<tr>
							<td class="times">4回</td>
							<td class="price">10,000円（2,500円／回）</td>
						</tr>
						<tr>
							<td class="times">2回</td>
							<td class="price">6,000円（3,000円／回）</td>
						</tr>
						<tr>
							<td class="space" colspan="3"></td>
						</tr>
						<tr>
							<td class="times">買い足し（1回分のみ）</td>
							<td class="price">2,500円</td>
							<td class="limit">当日</td>
						</tr>
						<tr>
							<td class="times">レンタルマット</td>
							<td class="price">無料</td>
							<td class="limit">-</td>
						</tr>
					</tbody>

				</table>
				<p class="payment">お支払い方法<br />
					クレジットカード VISA / Master / JCB<br />
					※買い足し（2,500円）は現金のみ。</p>
		    </section>

		    <section id="ticket">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/price/price05.png" alt="チケット会員"></h3>
				<p class="context">定期的に通うのが難しかったり、自分のペースでヨガを楽しみたいという方におすすめの回数券制です。<br />有効期限内のお好きな時にレッスンを受講していただけます。</p>
				<table>
					<tbody>
						<tr>
							<th class="times">&nbsp;</th>
							<th class="price">料金</th>
							<th class="limit">有効期限</th>
						</tr>
						<tr>
							<td class="times">8回</td>
							<td class="price">25,000円（3,125円／回）</td>
							<td class="limit">4ヶ月</td>
						</tr>
						<tr>
							<td class="times">4回</td>
							<td class="price">13,000円（3,250円／回）</td>
							<td class="limit">2ヶ月</td>
						</tr>
						<tr>
							<td class="times">レンタルマット</td>
							<td class="price">300円／回</td>
							<td class="limit">当日</td>
						</tr>
					</tbody>

				</table>
				<p class="payment">お支払い方法<br />
					クレジットカード VISA / Master / JCB　または　現金<br />
					※回数券は都度スタジオにてご購入いただけます。</p>

		    </section>

		    <section id="dropin">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/price/price06.png" alt="ドロップイン"></h3>
				<p class="context">ヨガをしたいと思ったらすぐにご利用いただけます。<br />なんとなく身体を動かしたい時や、急に予定が空いた場合などにご利用ください。</p>
				<table>
					<tbody>
						<tr>
							<th class="times">&nbsp;</th>
							<th class="price">料金</th>
							<th class="limit">有効期限</th>
						</tr>
						<tr>
							<td class="times">1回チケット</td>
							<td class="price">3,500円</td>
							<td class="limit">当日</td>
						</tr>
						<tr>
							<td class="times">レンタルマット</td>
							<td class="price">300円／回</td>
							<td class="limit">当日</td>
						</tr>
					</tbody>

				</table>
				<p class="payment">お支払い方法<br />
					現金</p>


		    </section>
<!--
		    <section id="siesta">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/price/price07.png" alt="笑いヨガ"></h3>
				<p class="context">気軽に笑いヨガを体験して頂けるよう特別プランをご用意いたしました。</p>
				<table>
					<tbody>
						<tr>
							<th class="times">&nbsp;</th>
							<th class="price">料金</th>
							<th class="limit">有効期限</th>
						</tr>
						<tr>
							<td class="times">1回チケット</td>
							<td class="price">1,500円</td>
							<td class="limit">当日限り</td>
						</tr>
					</tbody>

				</table>
				<p class="payment">お支払い方法<br />
					現金</p>


		    </section>
-->
		    <section id="trial">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/price/price08.png" alt="体験レッスン"></h3>
				<p class="context">スケジュールの中から参加希望のクラスを自由にお選びいただけます。<br />要事前予約。レッスン開始時間の10分前までにスタジオにお越しください。<br /><a href="register.html">詳しくは、体験レッスンからご入会までの流れのページへ。</a></p>
				<table>
					<tbody>
						<tr>
							<th class="times">&nbsp;</th>
							<th class="price">料金</th>
							<th class="limit">有効期限</th>
						</tr>
						<tr>
							<td class="times">1回体験チケット</td>
							<td class="price">1,500円</td>
							<td class="limit">当日</td>
						</tr>
						<tr>
							<td class="times">2回体験チケット</td>
							<td class="price">2,000円（1,000円／回）</td>
							<td class="limit">1週間</td>
						</tr>
						<tr>
							<td class="times">3回体験チケット</td>
							<td class="price">2,500円（840円／回）</td>
							<td class="limit">1週間</td>
						</tr>
						<tr>
							<td class="times">レンタルマット</td>
							<td class="price">無料</td>
							<td class="limit">-</td>
						</tr>
					</tbody>

				</table>
				<p class="payment">お支払い方法<br />
					現金</p>


		    </section>

		    <section id="private">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/price/price09.png" alt="プライベートレッスン・出張レッスン"></h3>
				<p class="context">マナヨガでは、プライベートレッスン、お友達やサークルなど仲間限定レッスン、ご家庭や企業への<br />出張レッスンも行っております。<br />ご希望の日時、場所、内容などご要望がございましたらお問い合わせください。<br /><a href="mailto:info@manayoga.jp">info@manayoga.jp</a></p>
				<h4>プライベートレッスン</h4>
				<table>
					<tbody>
						<tr>
							<th class="times">人数</th>
							<th class="price2">1時間あたりの料金</th>
						</tr>
						<tr>
							<td class="times">1名</td>
							<td class="price2">10,000円</td>
						</tr>
						<tr>
							<td class="times">2・3名</td>
							<td class="price2">10,000円（約3,300〜5,000円／人）</td>
						</tr>
						<tr>
							<td class="times">4〜8名</td>
							<td class="price2">20,000円（2,500〜5,000円／人）</td>
						</tr>
					</tbody>

				</table>
				<p class="context">キャンセルの際は、レッスン開催日の前日21:00までにご連絡ください。<br />それ以降のキャンセルとなる場合、また無断でのキャンセルの場合は、<br />キャンセル料としてレッスン料の50％をご請求いたします。<br /><br />
					お支払い方法<br />
					現金</p>
				<h4>出張レッスン</h4>
				<table>
					<tbody>
						<tr>
							<th class="times">人数・内容</th>
							<th class="price2">1レッスンあたりの料金</th>
						</tr>
						<tr>
							<td class="times">要相談</td>
							<td class="price2">10,000円〜</td>
						</tr>
					</tbody>

				</table>
				<p class="context">
					キャンセルの際は、レッスン開催日の前日の21:00までにご連絡ください。<br />それ以降のキャンセルとなる場合、また無断でのキャンセルの場合は、<br />キャンセル料としてレッスン料の50％をご請求いたします。<br /><br />
					お支払い方法<br />
					現金</p>


		    </section>




	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>