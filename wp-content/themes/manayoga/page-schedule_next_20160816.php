<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="schedule" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule01.png" alt="マナヨガ">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule02.png" alt="スケジュール"></h2>

		    <section id="move">
				<div id="previous" class="left"><a href="<?php echo home_url('/schedule/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule10.png" alt="前月"></a></div>
				<!--<div id="next" class="right"><a href="<?php echo home_url('/schedule_next/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule11.png" alt="翌月"></a></div>-->
				<div class="clearfix"></div>
		    </section>


		    <section id="mouth">
				<img src="<?php echo get_template_directory_uri(); ?>/img/schedule/2016month/09.png" alt="2016年09月">
		    </section>


		    <section id="mon" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule03.png" alt="月曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/277936" target="_blank">
							<time>10：00-11：00</time>
							<em>元気アロマヨガ</em>
							<name>Minori</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/145830" target="_blank">
							<time>13：00-14：00</time>
							<em>女子力アップヨガ（女性限定）</em>
							<name>IKUMI</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/228303" target="_blank">
							<time>19：30-20：30</time>
							<em>月のヨガ</em>
							<name>HARU（12日・26日）／福井れい（5日・19日）</name>
						</a>
					</li>
				</ul>
		    </section>

		    <section id="tue" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule04.png" alt="火曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/236578" target="_blank">
							<time>13：00-14：00</time>
							<em>カラダメンテナンス</em>
							<name>IKUMI</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/275489" target="_blank">
							<time>19：00-20：00</time>
							<em>ハタ＆リラックスヨガ</em>
							<name>エリコ</name>
						</a>
					</li>
				</ul>
		    </section>

		    <section id="wed" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule05.png" alt="水曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/352983" target="_blank">
							<time>7：30-8：15</time>
							<em>朝ヨガ</em>
							<name>福井れい</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/263311" target="_blank">
							<time>10：00-11：00</time>
							<em>優しいヨガ</em>
							<name">emi（7日・21日）<br></name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/105288" target="_blank">
							<time>19：30-20：30</time>
							<em>子宮美人ヨガ</em>
							<name>Hitomi（14日・21日））</name>
						</a>
					</li>
				</ul>
		    </section>

		    <section id="thu" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule06.png" alt="木曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/331360" target="_blank">
							<time>10：00-11：00</time>
							<em class="small">ファンクショナルローラーピラティス</em>
							<name>kasumi<br>（22日は休講となります）</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/356534" target="_blank">
							<time>13：00-14：00</time>
							<em>美脚ヨガ</em>
							<name>IKUMI（8日・22日）</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/329267" target="_blank">
							<time>19：30-20：30</time>
							<em>自分コントロールヨガ</em>
							<name>福井れい</name>
						</a>
					</li>
				</ul>
		    </section>

		    <section id="fri" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule07.png" alt="金曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/278960" target="_blank">
							<time>13：00-14：00</time>
							<em>カラダメンテナンス</em>
							<name>IKUMI</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/111162" target="_blank">
							<time>15：30-16：30</time>
							<em>優しいヨガ</em>
							<name>大橋真喜子（30日は休講となります）</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/214241" target="_blank">
							<time>19：30-20：30</time>
							<em>お疲れリリースヨガ</em>
							<name>ちひろ</name>
						</a>
					</li>
				</ul>
		    </section>

		    <section id="sat" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule08.png" alt="土曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/116765" target="_blank">
							<time>17：00-18：00</time>
							<em>For Men（男性限定）</em>
							<name>IKUMI</name>
						</a>
					</li>
				</ul>
		    </section>

		    <section id="sun" class="weekly">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule09.png" alt="日曜日"></h3>
				<ul>
					<li>
						<a href="https://coubic.com/manayogastudio/194621" target="_blank">
							<time>13：00-14：00</time>
							<em>カラダメンテナンス</em>
							<name>IKUMI</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/120660" target="_blank">
							<time>15：00-16：00</time>
							<em>お腹スッキリヨガ</em>
							<name>Hitomi（4日・18日）／りょうこ（11日・25日）</name>
						</a>
					</li>
					<li>
						<a href="https://coubic.com/manayogastudio/169716" target="_blank">
							<time>17：00-18：00</time>
							<em>自分コントロール</em>
							<name>福井れい</name>
						</a>
					</li>
				</ul>
		    </section>

			<section id="context1">
				<p>
					マナヨガは完全予約制をとっております。<br />
					レッスン開始3時間前までに予約システムよりご予約ください。<br />
					ウェアの無料貸出しも行っております。</p>
			</section>

			<section id="context2">
				<p>
					※20:00以降、ビルの1階正面入口が夜間施錠されております。<br />
					20:00以降のレッスンにご予約頂いた方にはアクセスコードをお送りさせて<br />
					頂きます。ご自身で解錠して頂き、スタジオまでお越しください。
				</p>
			</section>

	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>