<!DOCTYPE HTML>
<!--[if IE 6]> <html class="no-js lt-ie9 lt-ie8 lt-ie7 eq-ie6" lang="ja"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8 eq-ie7" lang="ja"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9 eq-ie8" lang="ja"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="ja"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
	<meta name="viewport" content="width=1024" />
    <title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css" type="text/css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css" type="text/css">
    <link rel="stylesheet" media="all" href="<?php echo get_stylesheet_uri(); ?>" type="text/css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
    <!--[if lt IE 9]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="js/html5shiv.js"></script><![endif]-->
    <!--[if lt IE 8]><script src="js/selectivizr.js"></script><![endif]-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-71105482-1', 'auto');
  ga('send', 'pageview');

</script>

<?php wp_head(); ?>
</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="band"></div>

	    <header>
		    <div>
		        <div class="left">
					<h1><a href="<?php echo home_url('/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/title.png"></a></h1>
		        </div>
		        <div class="right">
		        	<ul>
			        	<li><a href="<?php echo home_url('/register/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/trial.png" alt="体験レッスンを予約する"></a></li>
			        	<li><a href="https://coubic.com/manayogastudio/services"><img src="<?php echo get_template_directory_uri(); ?>/img/common/lesson.png" alt="レッスンを予約する"></a></li>
			        	<li>
			        		<p>恵比寿駅西口より歩いて2分!</p>
			        		<p>セミプライベートスタジオ</p>
			        	</li>
		        </div>
		        <div class="clearfix"></div>
		    </div>
	        <nav>
		        <ul>
			        <li><a href="<?php echo home_url('/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav01.png" alt="ホーム"></a></li>
			        <li><a href="<?php echo home_url('/schedule/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav02.png" alt="スケジュール"></a></li>
			        <li><a href="<?php echo home_url('/menu/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav03.png" alt="レッスンメニュー"></a></li>
			        <li><a href="<?php echo home_url('/price/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav04.png" alt="料金システム"></a></li>
			        <li><a href="<?php echo home_url('/instructor/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav05.png" alt="インストラクター紹介"></a></li>
			        <li><a href="<?php echo home_url('/register/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav06.png" alt="ご入会の流れ"></a></li>
			        <li><a href="<?php echo home_url('/access/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/nav07.png" alt="アクセス"></a></li>
		        </ul>
		        <div class="clearfix"></div>
	        </nav>

	    </header>
