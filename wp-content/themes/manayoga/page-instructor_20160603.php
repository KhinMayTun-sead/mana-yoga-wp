<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="instructor" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01.png">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor02.png" alt="インストラクター紹介"></h2>

		    <section id="list">
			    <ul>
				    <li><a href="<?php echo home_url('/instructor/instructor01/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor01.png" alt="IKUMI"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor02/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor02.png" alt="TAKAKO"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor03/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor03.png" alt="福井れい"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor04/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor04.png" alt="Minori"></a></li>
				    <!--<li><a href="<?php echo home_url('/instructor/instructor05/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor05.png" alt="megumi"></a></li>-->
				    <li><a href="<?php echo home_url('/instructor/instructor06/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor06.png" alt="エリコ"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor07/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor07.png" alt="Hitomi"></a></li>
<!--				    <li><a href="<?php echo home_url('/instructor/instructor08/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor08.png" alt="toco"></a></li>-->
				    <li><a href="<?php echo home_url('/instructor/instructor09/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor09.png" alt="HARU"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor10/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor10.png" alt="ひろこ"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor11/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor11.png" alt="しん"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor12/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor12.png" alt="大橋真喜子"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor13/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor13.png" alt="けいこ"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor14/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor14.png" alt="emi"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor15/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor15.png" alt="kasumi"></a></li>
<!--				    <li><a href="<?php echo home_url('/instructor/instructor16/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor16.png" alt="hitomi"></a></li>-->
				    <li><a href="<?php echo home_url('/instructor/instructor17/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor17.png" alt="ちひろ"></a></li>
				    <!--<li><a href="<?php echo home_url('/instructor/instructor18/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor18.png" alt="Kazumi"></a></li>-->
				    <li><a href="<?php echo home_url('/instructor/instructor19/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor19.png" alt="サトミ"></a></li>
				    <li><a href="<?php echo home_url('/instructor/instructor21/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor21.png" alt="MASA"></a></li>
			    </ul>

		    </section>





	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>