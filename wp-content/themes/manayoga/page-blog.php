<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="blog" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog01.png" alt="manayoga blog">
		    </section>

		    <section class="blog">
				<ul>
	            	<?php
					$newslist = get_posts( array(
					'posts_per_page' => 30, //取得記事件数
					'cat'            =>-2
					));
					foreach( $newslist as $post ):
					setup_postdata( $post );
					?>

					<li><a href="<?php the_permalink(); ?>"><date><?php echo get_the_date('Y年m月d日') ?></date><span><?php the_title(); ?></span></a></li>

	                <?php
					endforeach;
					wp_reset_postdata();
					?>

				</ul>

			    
		    </section>


	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>