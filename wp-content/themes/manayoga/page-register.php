<?php get_header(); ?>
		        
	<div class="wrap">

	    <article id="register" class="subpage">

		    <section id="photo">
			    <img src="<?php echo get_template_directory_uri(); ?>/img/register/register01.png">
		    </section>
		    
		    <h2><img src="<?php echo get_template_directory_uri(); ?>/img/register/register02.png" alt="体験レッスンからご入会までの流れ"></h2>



		    <section id="apply">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/register/register04.png" alt="体験レッスンに申し込む"></h3>
				<p class="context"><a href="<?php echo home_url('/schedule/'); ?>">レッスンスケジュール</a>を確認し、<a href="https://coubic.com/manayogastudio/services">専用の予約システム</a>より受けたいレッスンを選び、<br />予約してください。<br />
					<br />
					予約完了メールが届きます。<br />
					届かない場合は<a href="mailto:info@manayoga.jp">info@manayoga.jp</a>までご連絡ください。
				</p>

		    </section>


		    <section id="before">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/register/register05.png" alt="レッスン当日"></h3>
				<p class="context">レッスン開始10分前までにスタジオにお越しください。<br />
					場所は<a href="https://www.google.co.jp/maps/place/%E3%83%9E%E3%83%8A%E3%83%A8%E3%82%AC%E3%82%B9%E3%83%80%E3%82%B8%E3%82%AA/@35.6484834,139.7065481,17z/data=!3m1!4b1!4m2!3m1!1s0x60188b4176d91c3f:0xbb4d894d54ad8bb6?hl=ja" target="_brank">渋谷区恵比寿西1-2-1 エビスマンション706</a>。恵比寿駅西口から徒歩2分です。<br />
					<a href="<?php echo home_url('/access/'); ?>">アクセスページへ</a><br />
					<br />
					着替えやお荷物の保管については、インストラクターがご案内します。
				</p>
		    </section>

		    <section>
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/register/register06.png" alt="体験レッスン時の服装について"></h3>
				<p class="context">
<i>●</i>Tシャツ、タンクトップ、ジャージ、スパッツ、ハーフパンツなど、<br />
　動きやすい格好であれば大丈夫です。<br />
<i>●</i>レッスンは裸足で行いますので、靴は不要です。<br />
<i>●</i>ヨガマットはスタジオで無料でお貸出しいたします。<br />
<i>●</i>ウェアの無料貸出しも行っております。
				</p>

		    </section>



		    <section>
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/register/register07.png" alt="体験レッスンの料金"></h3>
				<table>
					<tbody>
						<tr>
							<th class="times">&nbsp;</th>
							<th class="price">料金</th>
							<th class="limit">有効期限</th>
						</tr>
						<tr>
							<td class="times">1回体験チケット</td>
							<td class="price">1,500円</td>
							<td class="limit">当日</td>
						</tr>
						<tr>
							<td class="times">2回体験チケット</td>
							<td class="price">2,000円（1,000円／回）</td>
							<td class="limit">1週間</td>
						</tr>
						<tr>
							<td class="times">3回体験チケット</td>
							<td class="price">2,500円（840円／回）</td>
							<td class="limit">1週間</td>
						</tr>
						<tr>
							<td class="times">レンタルマット</td>
							<td class="price">無料</td>
							<td class="limit">-</td>
						</tr>
					</tbody>

				</table>
				<p class="payment">お支払い方法<br />
					現金<br />
					理由の如何に関わらず、一度入金されたレッスン料は返金致しかねます。<br />またチケットは譲渡、転売はできかねます。</p>

		    </section>

		    <section>
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/register/register08.png" alt="オススメのクラス"></h3>
				<p class="context2">
マナヨガの体験レッスンは、全てのクラスの中からお選び頂けます。<br />
ヨガ未経験の方も、他にはないクラスをお探しの方も、気になるクラスをお気軽にお受けください。
				</p>
				<dl>
					<dt>ビギナー</dt>
					<dd>ヨガは初めて、という方大歓迎！<br />基礎から学べる、<br />入門・初級クラスです。</dd>
				</dl>
				<dl>
					<dt>優しいヨガ</dt>
					<dd>ひとつひとつのポーズを<br />ゆっくり丁寧にお伝えしていきます。<br />初心者の方も安心の<br />優しいレッスンです。</dd>
				</dl>
				<dl class="end">
					<dt>お疲れリリースヨガ</dt>
					<dd>ゆったり心地よく身体を<br />伸ばしたり捩じったりしながら、<br />血流やリンパを刺激して<br />疲労物質を流していきます。</dd>
				</dl>
				<dl>
					<dt>お腹スッキリヨガ</dt>
					<dd>体幹を強くして<br />ポッコリお腹を解消、<br />内臓にも働きかけて<br />デトックス効果を高めます。</dd>
				</dl>
				<dl>
					<dt>パワー</dt>
					<dd>運動量が多く、ダイナミック、ハードな動きを呼吸に合わせて行います。<br />身体を動かしたい！<br />という方にはピッタリです。
</dd>
				</dl>
				<dl class="end">
					<dt>For Men</dt>
					<dd>ヨガに興味はあるけど…そんな<br />男性のための男性限定クラスです。<br />しっかり動いて<br />締まった身体を目指します。</dd>
				</dl>
				<div class="clearfix"></div>


		    </section>


		    <section id="join">
				<h3><img src="<?php echo get_template_directory_uri(); ?>/img/register/register09.png" alt="ご入会の手続き"></h3>
				<p class="context">体験レッスン終了後、ご入会についての説明をさせていただきます。<br />
					ご質問がございましたら、何でもおたずねください。<br />
					体験レッスンの最終日にご入会いただくと、入会金が無料になる特典もございます。<br />
				</p>
		    </section>




	    </article>
	
<?php get_sidebar(); ?>
	
	</div><!-- /wrap -->	
	

<?php get_footer(); ?>