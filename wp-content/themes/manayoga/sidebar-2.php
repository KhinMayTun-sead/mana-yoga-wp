		<aside>
			<ol>
				<li><a href="<?php echo home_url('/event/'); ?>"><span>▶</span> イベント・WS情報</a></li>
				<li><a href="<?php echo home_url('/canceledlecture/'); ?>"><span>▶</span> 代行・休講情報</a></li>
				<li><a href="<?php echo home_url('/lesson/'); ?>"><span>▶</span> レッスンスケジュール</a></li>
				<li><a href="<?php echo home_url('/campaign/'); ?>"><span>▶</span> キャンペーン情報</a></li>
				<li><a href="<?php echo home_url('/diary/'); ?>"><span>▶</span> マナヨガ日記</a></li>
			</ol>
				
			<ul>
				<li><a href="<?php echo home_url('/register/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side01.png"></a></li>
				<li><a href="<?php echo home_url('/faq/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side02.png"></a></li>
				<li><a href="<?php echo home_url('/access/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side03.png"></a></li>
				<li><a href="<?php echo home_url('/photogallery/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side04.png"></a></li>
				<li><a href="mailto:info@manayoga.jp"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side05.png"></a></li>
				<li><p>TEL:03-6455-1756</p><p><span>※レッスン中はお電話に出られない場合もございますの<br />でご容赦ください。</span></p></li>
				<li><a href="<?php echo home_url('/blog/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side07.png"></a></li>
				<li><div class="fb-page" data-href="https://www.facebook.com/manayogastudio" data-width="280" data-height="130" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/manayogastudio"><a href="https://www.facebook.com/manayogastudio">マナヨガ</a></blockquote></div></div></li>
				<li><a href="http://www.yogaroom.jp" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side09.png"></a></li>
				<li><a href="http://www.yoga-station.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side10.png"></a></li>
				<li><a href="<?php echo home_url('/photogallery/'); ?>#aumnie"><img src="<?php echo get_template_directory_uri(); ?>/img/common/side11.png"></a></li>
				<li><img src="<?php echo get_template_directory_uri(); ?>/img/common/side12.png"></li>
			</ul>
		</aside>

		<div class="clearfix"></div>
