<section class="element inner">
  <div class="booking">
    <a href="<?php echo home_url('/register/'); ?>">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/booking.png" alt="booking">
    </a>
  </div>
  <div class="contact mb20">
    <a href="mailto:info@manayoga.jp">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/mail.png" alt="mail">
    </a>
  </div>
  <div class="cont-02">
    <h3>ＴＥＬ：０３-６４５５-１７５６</h3>
    <p>※レッスン中はお電話に出られない場合もございますのでご容赦ください。</p>
  </div>
<div class="gallery">
  <ul>
    <li><a href="<?php echo home_url('/blog/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/img_01.png" alt=""></a></li>
    <li class="fb_center"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmanayogastudio&tabs&width=340&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId" width="680" height="240" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></li>
    <li><a href="http://www.yogaroom.jp"><img src="<?php echo get_template_directory_uri(); ?>/common/images/img_03.png" alt=""></a></li>
    <li><a href="http://www.yoga-station.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/common/images/img_04.png" alt=""></a></li>
    <li><a href="<?php echo home_url('/photogallery/'); ?>#aumnie"><img src="<?php echo get_template_directory_uri(); ?>/common/images/img_05.png" alt=""></a></li>
    <li><img src="<?php echo get_template_directory_uri(); ?>/common/images/img_06.png" alt=""></li>
  </ul>
</div>
<a href="#top" class="totop"><img src="<?php echo get_template_directory_uri(); ?>/common/images/top.png" alt="↑" /></a>
</section>
</main>
<footer>
<ul class="ft-list mb40 cf">
  <li><a href="<?php echo home_url('/schedule/'); ?>">スケジュール</a></li>
  <li><a href="<?php echo home_url('/menu/'); ?>">レッスンメニュー</a></li>
  <li><a href="<?php echo home_url('/price/'); ?>">料金システム</a></li>
  <li><a href="<?php echo home_url('/instructor/'); ?>">インストラクター紹介</a></li>
  <li><a href="<?php echo home_url('/access/'); ?>">アクセス</a></li>
  <li><a href="<?php echo home_url('/register/'); ?>">ご利用の流れ</a></li>
  <li><a href="<?php echo home_url('/register/'); ?>">体験レッスンを予約する</a></li>
  <li><a href="<?php echo home_url('/faq/'); ?>">よくある質問</a></li>
  <li><a href="<?php echo home_url('/photogallery/'); ?>">フォトギャラリー</a></li>
  <li><a href=""></a></li>
</ul>
<a href="<?php echo home_url(); ?>" class="ft-logo"><img src="<?php echo get_template_directory_uri(); ?>/common/images/footer_logo.png" alt="Mana Yoga" class="mb40"></a>
<ul class="blog inner mb40 cf">
  <li class="lit-01"><a href="<?php echo home_url('/blog/'); ?>">manayoga blog</a></li>
  <li class="lit-02"><a href="<?php echo home_url('/law/'); ?>">特定商取引法に基づく表記</a></li>
  <li class="last"><a href="<?php echo home_url('/policy/'); ?>">プライバシーポリシー</a></li>
</ul>
<img src="<?php echo get_template_directory_uri(); ?>/common/images/footer_yoga.png" alt="" class="mb20">
<p>Copyright © 2016 ManaYoga All Rights Reserved.</p>
</footer>
</div>
<!-- wrapper -->

<?php wp_footer(); ?>
</body>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/common.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/common/js/smoothScroll.js"></script>
</html>
