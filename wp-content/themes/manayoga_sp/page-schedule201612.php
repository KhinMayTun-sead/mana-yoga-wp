<?php get_header(); ?>

<div class="banner mb40">
  <img src="<?php echo get_template_directory_uri(); ?>/img/schedule/schedule_bnr_img.png" alt="">
</div>

<section class="schedule-box inner">
  <a href="<?php echo home_url('/schedule_next/'); ?>" class="next-sch"><img src="<?php echo get_template_directory_uri(); ?>/common/images/next-schedule-img01.png" alt=""></a>
  <h2 class="sch-year"><img src="<?php echo get_template_directory_uri(); ?>/common/images/year-2016-dec.png" alt=""></h2>
  <div id="monday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-monday.png" alt=""></h3>
    <ul>
      <li>
        <a href="https://coubic.com/manayogastudio/277936" target="_blank" class="cf">
          <span class="sch-time">10：00-11：00</span>
          <span class="sch-class-name">元気アロマヨガ</span>
          <span class="sch-instructor">Minori</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/228303" target="_blank" class="cf">
          <span class="sch-time">19：30-20：30</span>
          <span class="sch-class-name">月のヨガ</span>
          <span class="sch-instructor">HARU（12日・26日）／福井れい（5日・19日）<br>（5日・19日は休講となります）</span>
        </a>
      </li>
    </ul>
  </div>
  <div id="tuesday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-tuesday.png" alt=""></h3>
    <ul>
      <li>
        <a href="https://coubic.com/manayogastudio/236578" target="_blank" class="cf">
          <span class="sch-time">13：00-14：30</span>
          <span class="sch-class-name">カラダメンテナンス</span>
          <span class="sch-instructor">IKUMI</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/275489" target="_blank" class="cf">
          <span class="sch-time">19：00-20：00</span>
          <span class="sch-class-name">ハタ＆リラックスヨガ</span>
          <span class="sch-instructor">エリコ（20日は休講となります）</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/138745" target="_blank" class="cf">
          <span class="sch-time">20：30-21：30</span>
          <span class="sch-class-name">ベーシックヨガ</span>
          <span class="sch-instructor">IKUMI（20日・27日は休講となります）</span>
        </a>
      </li>
    </ul>
  </div>
  <div id="wednesday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-wednesday.png" alt=""></h3>
    <ul>
      <li>
        <a href="https://coubic.com/manayogastudio/263311" target="_blank" class="cf">
          <span class="sch-time">10：00-11：00</span>
          <span class="sch-class-name">優しいヨガ</span>
          <span class="sch-instructor">IKUMI</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/307734" target="_blank" class="cf">
          <span class="sch-time">12：00-13：00</span>
          <span class="sch-class-name">シニアヨガ</span>
          <span class="sch-instructor">IKUMI</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/105288" target="_blank" class="cf">
          <span class="sch-time">19：30-20：30</span>
          <span class="sch-class-name">子宮美人ヨガ </span>
          <span class="sch-instructor">Hitomi（14日・21日）</span>
        </a>
      </li>
    </ul>
  </div>
  <div id="thursday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-thursday.png" alt=""></h3>
    <ul>
      <li>
        <a href="https://coubic.com/manayogastudio/384281" target="_blank" class="cf">
          <span class="sch-time">7：30-8：15</span>
          <span class="sch-class-name">朝ヨガ</span>
          <span class="sch-instructor">kasumi</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/331360" target="_blank" class="cf">
          <span class="sch-time">10：00-11：00</span>
          <span class="sch-class-name">ファンクショナルローラーピラティス</span>
          <span class="sch-instructor">kasumi</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/356534" target="_blank" class="cf">
          <span class="sch-time">13：00-14：00</span>
          <span class="sch-class-name">美脚ヨガ</span>
          <span class="sch-instructor">IKUMI</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/329267" target="_blank" class="cf">
          <span class="sch-time">19：30-20：30</span>
          <span class="sch-class-name">自分コントロールヨガ</span>
          <span class="sch-instructor">福井れい</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/216024" target="_blank" class="cf">
          <span class="sch-time">21：00-22：00</span>
          <span class="sch-class-name">For Men（男性限定）</span>
          <span class="sch-instructor">IKUMI（22日は休講となります）</span>
        </a>
      </li>
    </ul>
  </div>
  <div id="friday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-friday.png" alt=""></h3>
    <ul>
      <li>
        <a href="https://coubic.com/manayogastudio/278960" target="_blank" class="cf">
          <span class="sch-time">13：00-14：30</span>
          <span class="sch-class-name">カラダメンテナンス</span>
          <span class="sch-instructor">IKUMI</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/214241" target="_blank" class="cf">
          <span class="sch-time">19：30-20：30</span>
          <span class="sch-class-name">お疲れリリースヨガ</span>
          <span class="sch-instructor">ちひろ</span>
        </a>
      </li>
    </ul>
  </div>
  <div id="saturday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-saturday.png" alt=""></h3>
    <ul>
      <li>
        <a href="https://coubic.com/manayogastudio/116765" target="_blank" class="cf">
          <span class="sch-time">17：00-18：00</span>
          <span class="sch-class-name">For Men（男性限定）</span>
          <span class="sch-instructor">IKUMI（17日は休講となります）</span>
        </a>
      </li>
    </ul>
  </div>
  <div id="sunday" class="sch-list">
    <h3><img src="<?php echo get_template_directory_uri(); ?>/common/images/date-sunday.png" alt=""></h3>
    <ul>
      <li>
        <a href="http://www.manayoga.jp/uncategorized/242/" target="_blank" class="cf">
          <span class="sch-time">10：00-11：30</span>
          <span class="sch-class-name">進化ウォーキングイベント</span>
          <span class="sch-instructor">kasumi（4日のみ開催）</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/263311" target="_blank" class="cf">
          <span class="sch-time">10：00-11：30</span>
          <span class="sch-class-name">ファンクショナルローラーピラティス</span>
          <span class="sch-instructor">kasumi（18日のみ開催）</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/194621" target="_blank" class="cf">
          <span class="sch-time">13：00-14：00</span>
          <span class="sch-class-name">カラダメンテナンス</span>
          <span class="sch-instructor">IKUMI（4日は休講となります）</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/120660" target="_blank" class="cf">
          <span class="sch-time">15：00-16：00</span>
          <span class="sch-class-name">肩懲り解消ヨガ</span>
          <span class="sch-instructor">Hitomi（4日・18日）／りょうこ（11日・25日）</span>
        </a>
      </li>
      <li>
        <a href="https://coubic.com/manayogastudio/169716" target="_blank" class="cf">
          <span class="sch-time">17：00-18：00</span>
          <span class="sch-class-name">自分コントロール</span>
          <span class="sch-instructor">福井れい</span>
        </a>
      </li>
    </ul>
  </div>
  <div class="remark-txt">
    <ul>
      <li>マナヨガは完全予約制をとっております。</li>
      <li>レッスン開始3時間前までに予約システムよりご予約ください。</li>
      <li>ウェアの無料貸出しも行っております。</li>
    </ul>
  </div>
  <div class="remark-txt col-red">
    <ul>
      <li>※20:00以降、ビルの1階正面入口が夜間施錠されております。</li>
      <li>20:00以降のレッスンにご予約頂いた方にはアクセスコードをお送りさせて<br>頂きますご自身で解錠して頂き、スタジオまでお越しください。</li>
    </ul>
  </div>
</section>

<section class="instructor inner">
  <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01_img10.png">
  <div class="autoplay">
    <?php instructor_list(); ?>
  </div>
</section><!-- instructor-->

<?php get_footer(); ?>
