<?php get_header(); ?>

<section class="blog-01 inner">
  <div class="photo">
    <img src="<?php echo get_template_directory_uri(); ?>/common/images/blog01.png" alt="blog">
  </div>
  <div class="information">
    <ul>
      <?php
      $newslist = get_posts( array(
      'posts_per_page' => 30, //取得記事件数
      'cat'            =>-2
      ));
      foreach( $newslist as $post ):
      setup_postdata( $post );
      ?>

      <li class="cf"><a href="<?php the_permalink(); ?>" class="cf"><span class="info-date"><?php echo get_the_date('Y年m月d日') ?></span><span class="info-txt"><?php the_title(); ?></span></a></li>

              <?php
      endforeach;
      wp_reset_postdata();
      ?>

    </ul>
  </div>
</section>

<section class="instructor inner">
  <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01_img10.png">
  <div class="autoplay">
    <?php instructor_list(); ?>
  </div>
</section><!-- instructor-->

<?php get_footer(); ?>
