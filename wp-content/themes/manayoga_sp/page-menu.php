<?php get_header(); ?>

<div class="banner mb40">
  <img src="<?php echo get_template_directory_uri(); ?>/img/menu/menu01.jpg" alt="レッスンメニュー">
</div>

<section class="content mb40 inner">
  <p class="mb40">マナヨガでは、初心者の方から本格的にヨガに取り組んでいる方まで、幅広いクラスを用意しています。リラクゼーション効果の高いゆったりクラス、気持ちよく汗をかくパワー系クラス、さらにマタニティやMen'sクラスなどのスペシャルクラスもあります。
    ご自身の体調やその日の気分、その時の目的に合わせて自由にクラスをお選びいただけます。</p>
</section>

<section class="yoga-lesson inner">
  <div class="menu menu01 mb40">
    <h3>ビギナーズヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>「ヨガは初めて！」という方、大歓迎！基礎から学べる、入門・初級クラスです。</p>
  </div>
  <div class="menu menu01 mb40">
    <h3>ベーシックヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>ヨガの基本のアーサナ(ポーズ)を中心としたクラス。</p>
  </div>
  <div class="menu menu02 mb40">
    <h3>優しいヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>日常生活ではなかなか意識して伸ばすことのない身体を、呼吸とともに伸ばしていくことで、気持ち良さを実感できます。<br />
      軽減ポーズも取り入れながら、一つ一つのポーズをゆっくり丁寧にお伝えしていきます。</p>
    </div>
  <div class="menu menu02 mb40">
    <h3>自分コントロールヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>このクラスではヨガの呼吸法とポーズ、瞑想を行い、ココロとカラダの両方からアプローチをしていきます。<br />
      ※動きの時間は若干少なめですが、強弱はお好みでアレンジ可能です。</p>
  </div>
  <div class="menu menu03 mb40">
    <h3>元気アロマヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>アロマの心地よい香りが呼吸を深めるのを助け、
      心身の緊張をほぐして意識を内側へと向けてくれます。</p>
    </div>
  <div class="menu menu04 mb40">
    <h3>月のヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength02.png" alt="" class="strength">
    </div>
    <p>自然界のリズム、そして月の満ち欠けは私たちの身体や心に大きな影響を与えます。新月には”締める”、満月には”緩める”など、月の満ち欠けに合わせたヨガで、心身を整えていきます。</p>
  </div>
  <div class="menu menu05 mb40">
    <h3>ヨガニードラ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>ヨガニードラの「ニードラ」とは「眠り」という意味で、シャバーサナ（休息のポーズ）の状態で行うヨガになります。インストラクターの誘導を聞きながら、心身を深くリラックスさせていきます。また、まどろんだ半覚の醒状態で「サンカルパ（決意）」を思い浮かべることで、潜在意識に働きかけ自己実現力を高めます。</p>
  </div>
  <div class="menu menu06 mb40">
    <h3>フロー</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength02.png" alt="" class="strength">
    </div>
    <p>呼吸のリズムに合わせ、アーサナ(ポーズ)をつないで流れるように行います。</p>
  </div>
  <div class="menu menu07 mb40">
    <h3>カラダメンテナンス</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength03.png" alt="" class="strength">
    </div>
    <p>身体の深部の筋肉（インナーマッスル）に働きかけ、体幹（コア）を鍛えていきます。代謝UPで身体を締めよう！！</p>
  </div>
  <div class="menu menu08 mb40">
    <h3>フィジカルヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength03.png" alt="" class="strength">
    </div>
    <p>筋肉や関節など、体の仕組みからヨガのアーサナを考え、より体に効果のあるヨガクラスを目指します。
      プロップスも使いながら、1人ひとりのからだの特徴に合わせたアーサナの取り方を提案し、深めていきます。</p>
  </div>
  <div class="menu menu08 mb40">
    <h3>アナトミック骨盤ヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength03.png" alt="" class="strength">
    </div>
    <p>キツイ！ でも効く！！<br />
      ゆっくりとした動きでジックリ筋肉を動かしで骨盤周辺、股関節周辺の筋肉を活性化させて柔軟性と筋力を養います！体型を整え、引き締める、自力整体のようなヨガを目指しています。</p>
  </div>
  <div class="menu menu09 mb40 ladys">
    <h3>子宮美人ヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>身体の緩める・締める、のバランスを整えていくクラスです。身体の軸を意識して楽に立てる姿勢をご紹介していきます。生理痛・PMS・尿漏れなど女性特有の悩みにアプローチしていきます。<br>
      女性のための身体づくりはじまめしょう♪</p>
  </div>
  <div class="menu menu03 mb40">
    <h3>ママヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p class="mt20">お子様も時にはパパも一緒にスタジオにいらっしゃってください。<br />安心できる環境の中でヨガを楽しみましょう。</p>
  </div>
  <div class="menu menu11 mb40 ladys">
    <h3>女子力UPヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength02.png" alt="" class="strength">
    </div>
    <p>日頃のコリをほぐし→内側の筋肉を締める→穏やかにリラックスするという3ステップ。<br />
      憧れるような引き締まったBODY!<br />
      そして女性らしいしなやかな心とカラダを作っていきます。</p>
  </div>
  <div class="menu menu12 mb40">
    <h3>ファンクショナルローラーピラティス</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength02.png" alt="" class="strength">
    </div>
    <p class="mb40">ファンクショナルローラーピラティス®（FRP）とは、理学療法士でピラティスインストラクターである中村尚人氏が考案した医学的根拠に基づいたピラティスです。</p>
    <ul class="cf">
      <li class="fl-l"><img src="<?php echo get_template_directory_uri(); ?>/img/menu/menu16.jpg" alt=""></li>
      <li class="fl-r"><p>ファンクショナルローラーピラティス®（FRP）とは、理学療法士でピラティスインストラクターである中村尚人氏が考案した医学的根拠に基づいたピラティスです。</p></li>
    </ul>
  </div>
  <div class="menu menu10 mb40">
    <h3>朝ヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength02.png" alt="" class="strength">
    </div>
    <p>ヨガで朝活しませんか？<br />
      45分のレッスンでスッキリ目覚めて１日を充実させましょう。</p>
  </div>
  <div class="menu menu13 mb40">
    <h3>お昼寝ヨガ</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>レッスンは出入り自由　仕事のお昼休憩に少し身体を動かしてストレッチしたい。午後からの仕事のために少し横になりたい。<br />各々自由に時間を過ごしていただけます。</p>
  </div>
  <div class="menu menu14 mb40 ladys">
    <h3>For women</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength01.png" alt="" class="strength">
    </div>
    <p>しなやかで健やかな女性らしい身体と心を作っていきます。疲れやストレスで固まったりゆがんだりしているところを、緩め、しっかりと鍛え、整えていくことで、不調を改善し、すっきりと気持ちの良い日々を送れるように！</p>
  </div>
  <div class="menu menu15 mb40 mens">
    <h3>For men</h3>
    <div class="lesson-menu">
      <img src="<?php echo get_template_directory_uri(); ?>/img/menu/strength02.png" alt="" class="strength">
    </div>
    <p>ヨガに興味はあるけど…<br />
      そんな男性のための男性限定クラスです。<br />
      しっかり動いて締まった身体を目指します。</p>
  </div>
</section>

<section class="instructor inner">
  <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01_img10.png">
  <div class="autoplay">
    <?php instructor_list(); ?>
  </div>
</section><!-- instructor-->

<?php get_footer(); ?>
