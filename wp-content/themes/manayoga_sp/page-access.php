<?php get_header(); ?>

<div class="banner mb40">
  <img src="<?php echo get_template_directory_uri(); ?>/img/access/access01.jpg" alt="スタジオへのアクセス">
</div>
<section class="map mb40 inner">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1621.0789035824832!2d139.70764785591174!3d35.64848343937602!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188b4176d91c3f%3A0xbb4d894d54ad8bb6!2z44Oe44OK44Oo44Ks44K544OA44K444Kq!5e0!3m2!1sja!2sjp!4v1452220145330" width="100%" height="440" frameborder="0" style="border:0" allowfullscreen=""></iframe>
</section>
<section class="inner">
  <img src="<?php echo get_template_directory_uri(); ?>/img/access/access02.jpg" alt="" class="mb40">
  <div class="info mb40">
    <h3>マナヨガ</h3>
    <dl class="cf">
      <dt class="fl-l">住&nbsp;&nbsp;&nbsp;所</dt>
      <dd class="fl-l">〒150-0021<br>
          東京都渋谷区恵比寿西1-2-1<br>
          エビスマンション706</dd>
      <dt class="fl-l">電話番号</dt>
      <dd class="fl-l">03-6455-1756</dd>
      <dt class="fl-l">営業時間</dt>
      <dd class="fl-l">7：00～22：30</dd>
    </dl>
    <p>※休館日はお問い合わせください。<br>
      ※セキュリティの関係上、当ビルは20：00以<br>
　           降入口が施錠されます。解錠方法はお問い合<br>
　           わせください。<br>
      ※レッスン中は電話に出られない場合もござい<br>　ますのでご容赦ください。</p>
  </div>
</section>
<section class="bg-gray mb40">
  <div class="inner">
    <div class="street">
      <img src="<?php echo get_template_directory_uri(); ?>/img/access/access03.jpg" alt="">
      <p class="mb40">恵比寿西口のエビスさん横、交番前の信号を渡る。<br>
      駒沢通りと垂直に走るソフトバンク（向かって右手）と野郎ラーメン（向かって左手）がある道をそのまま直進。</p>
    </div>
    <div class="street">
      <img src="<?php echo get_template_directory_uri(); ?>/img/access/access04.jpg" alt="">
      <p class="mb40">20ｍほど進むと右手に韓国料理ひまわりがあります。<br>
      そこの２差路を右に曲がり、線路側に進んでください。<br>
      線路に沿って30ｍほど直進すると<br>
      左手にエビスマンションの入り口がございます。</p>
    </div>
    <div class="street street01">
      <img src="<?php echo get_template_directory_uri(); ?>/img/access/access05.jpg" alt="">
      <p class="mb40">※ココカラファインのビルの7Fですが、<br>
      線路側に回らないとビルの入り口が<br>
      ございませんのでご注意くださいませ。</p>
    </div>
  </div>
</section>

<?php get_footer(); ?>
