<!doctype html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>恵比寿の小さなヨガ教室「マナヨガ」</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="keywords" content="マナヨガ,ヨガ,恵比寿,スタジオ,教室,スクール">
  <meta name="description" content="ヨガをはじめるなら恵比寿の小さなヨガ教室マナヨガへ。恵比寿駅西口から徒歩2分。インストラクターは全員有資格者。レッスンは少人数制なので、ヨガ初心者の方から中・上級者の方までひとりひとりしっかりサポートいたします。">
  <meta property="og:type" content="website">
  <meta property="fb:app_id" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta property="og:title" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">
 <!--  <link rel="shortcut icon" href="share/img/favicon.ico" />
 <link rel="apple-touch-icon" sizes="152x152" href="/share/img/favicon.ico"> -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/common/css/normalize.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/common/css/common.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/common/css/top.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/common/css/style.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/common/css/slick-theme.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/common/css/slick.css">
</head>

<body id="top" class="index">
  <div class="wrapper">
    <header class="head">
      <div class="band">
        <img src="<?php echo get_template_directory_uri(); ?>/common/images/band.jpg" alt="band">
      </div>
      <div class="top-head mb25 inner">
        <div class="top-head-inner cf">
          <h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/yoga_logo.png" alt="yoga"></a></h1>
        </div>
        <div class="nav-menu cf">
          <div class="test cf">
          <a class="panel-btn cf">
            <span class="border"></span>
            <span class="border"></span>
            <span class="border"></span>
          </a>
          <img src="<?php echo get_template_directory_uri(); ?>/common/images/nav_menu.png" alt="" class="fl-r">
          </div>
        </div>
        <nav class="g-nav">
            <ul>
              <li><a href="<?php echo home_url('/'); ?>">ホーム<br><span>HOME</span></a></li>
              <li><a href="<?php echo home_url('/schedule/'); ?>">スケジュール<br><span>SCHEDULE</span></a></li>
              <li><a href="<?php echo home_url('/menu/'); ?>">レッスンメニュー<br><span>LESSON MENU</span></a></li>
              <li><a href="<?php echo home_url('/price/'); ?>">料金システム<br><span>PRICE</span></a></li>
              <li><a href="<?php echo home_url('/instructor/'); ?>">インストラクター紹介<br><span>INSTRUCTOR</span></a></li>
              <li><a href="<?php echo home_url('/register/'); ?>">ご利用の流れ<br><span>HOW TO REGISTER</span></a></li>
              <li><a href="<?php echo home_url('/access/'); ?>">アクセス<br><span>ACCESS</span></a></li>
            </ul>
            <div class="nav-reserve inner cf">
              <a href="https://coubic.com/manayogastudio/services" class="nav-resv-01" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/common/images/reserve.png" alt="reserve">
              </a>
              <a href="<?php echo home_url('/register/'); ?>" class="nav-resv-02">
                <img src="<?php echo get_template_directory_uri(); ?>/common/images/reserve_02.png" alt="reserve-02">
              </a>
            </div>
        </nav>
      </div><!--top_head-->
      <div class="bott-head mb30">
        <div class="bott-head-inner cf">
          <a href="https://coubic.com/manayogastudio/services" class="resv-01" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/common/images/reserve.png" alt="reserve">
          </a>
          <a href="<?php echo home_url('/register/'); ?>" class="resv-02">
            <img src="<?php echo get_template_directory_uri(); ?>/common/images/reserve_02.png" alt="reserve-02">
          </a>
        </div>
      </div><!--bott_head-->
    </header>
