<?php get_header(); ?>

<section class="blog-cont inner">
  <div class="photo">
    <img src="<?php echo get_template_directory_uri(); ?>/img/blog/blog01.png" alt="manayoga blog">
  </div>

           <?php if (have_posts()) :
  while (have_posts()) : the_post(); ?>

  <div class="cont">
    <div class="descript">
        <p class="desc-date"><?php echo get_the_date('Y年m月d日') ?></p>
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php the_content() ?>
    </div>

    <div class="next-page">
      <div class="nxt-inner cf">
        <?php if( get_previous_post() ): ?>
        <div class="left fl-l"><?php previous_post_link('%link', '<img src="' .get_template_directory_uri(). '/common/images/blog03.png" alt="前の記事">'); ?></div>
        <?php endif;
        if( get_next_post() ): ?>
        <div class="right fl-r"><?php next_post_link('%link', '<img src="' .get_template_directory_uri(). '/common/images/blog04.png" alt="次の記事">'); ?></div>
        <?php endif; ?>
      </div>
    </div>

    <?php endwhile; // 繰り返し処理終了
    else : // ここから記事が見つからなかった場合の処理 ?>
    	<h2>記事はありません</h2>
    	<p>お探しの記事は見つかりませんでした。</p>
    <?php endif; ?>

    <div class="ta-right cf"><a href="<?php echo home_url('/blog/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/blog02.png" alt="一覧を見る"></a></div>

    <div class="new-article">
      <h2><p>新着記事</p></h2>
      <ul>
              <?php
        $newslist = get_posts( array(
        'posts_per_page' => 5, // 取得記事件数
        'cat'            =>-2
        ));
        foreach( $newslist as $post ):
        setup_postdata( $post );
        ?>

        <li class="cf"><a href="<?php the_permalink(); ?>" class="cf"><span class="info-date"><?php echo get_the_date('Y年m月d日') ?></span><span class="info-txt"><?php the_title(); ?></span></a></li>

                <?php
        endforeach;
        wp_reset_postdata();
        ?>

      </ul>
    </div>
  </div>
</section>

<section class="instructor inner">
  <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01_img10.png">
  <div class="autoplay">
    <?php instructor_list(); ?>
  </div>
</section><!-- instructor-->

<?php get_footer(); ?>
