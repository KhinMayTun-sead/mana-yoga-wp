<?php get_header(); ?>

<div class="banner mb40">
  <img src="<?php echo get_template_directory_uri(); ?>/img/price/price_bnr_img.jpg" alt="">
</div>
<section class="price-fee inner">
  <div class="all-rate mb40">
    <span>※料金はすべて税込価格です。</span>
    <p class="mb40">ひと月にレッスンに参加し放題の「フリーパス」や ご利用頻度に合わせてご購入いただける「回数チケット」をご用意しております。<br>ご自身のライフスタイルに合わせて、お選びください。</p>
  </div>
  <div class="enroll-fee">
    <h2>入会登録料　無料！</h2>
    <p>※体験レッスン終了日に入会の申し込みを される場合に限ります。</p>
    <div class="admin-fee">
      <p>通常5,000円</p>
      <p>※事務手数料・カード発行手数料含む</p>
    </div>
  </div>
</section>
<section class="trail-lesson inner">
  <div class="trail-box01">
    <h2>マンスリー会員</h2>
    <p class="mb20">じっくり続けてレッスンに通いたいという方へおすすめのおトクな月謝制度です。 毎月、設定した回数内でお好きな時に受講していただけます。</p>
    <table class="trail-table-s01">
      <tr>
        <th></th>
        <th>料金</th>
        <th>有効期限</th>
      </tr>
      <tr>
        <td>2回</td>
        <td>6,000円<br>(3,000円/回)</td>
        <td rowspan="4">当月</td>
      </tr>
      <tr>
        <td>4回</td>
        <td>10,000円<br>(2,500円/回)</td>
      </tr>
      <tr>
        <td>8回</td>
        <td>18,000円<br>(2,250円/回)</td>
      </tr>
      <tr>
        <td>フリーパス</td>
        <td>25,000円</td>
      </tr>
    </table>
    <table class="trail-table-s01">
      <tr>
        <td>買い足し<br>(1回分のみ)</td>
        <td>2,500円</td>
        <td>当日</td>
      </tr>
      <tr>
        <td>レンタルマット</td>
        <td>無料</td>
        <td>-</td>
      </tr>
    </table>
    <p>お支払方法</p>
    <p>クレジットカード VISA / Master / JCB</p>
    <p>※買い足し(2,500円)は現金のみ。</p>
  </div>
  <div class="trail-box01">
    <h2>チケット会員</h2>
    <table class="trail-table-s01">
      <tr>
        <th></th>
        <th>料金</th>
        <th>有効期限</th>
      </tr>
      <tr>
        <td>8回</td>
        <td>25,000円<br>(3,125円/回)</td>
        <td>4ヵ月</td>
      </tr>
      <tr>
        <td>4回</td>
        <td>13,000円<br>(3,250円/回)</td>
        <td>2ヵ月</td>
      </tr>
      <tr>
        <td>レンタルマット</td>
        <td>300円/回</td>
        <td>当日</td>
      </tr>
    </table>
    <p>お支払方法</p>
    <p>クレジットカード VISA / Master / JCB</p>
    <p>または現金</p>
    <p>※回数券は都度スタジオにてご購入いただけます。</p>
  </div>
  <div class="trail-box01">
    <h2>ドロップイン</h2>
    <p class="mb20">ヨガをしたいと思ったらすぐにご利用いただけます。<br>なんとなく身体を動かしたい時や、急に予定が空いた場合などにご利用ください。</p>
    <table class="trail-table-s01">
      <tr>
        <th></th>
        <th>料金</th>
        <th>有効期限</th>
      </tr>
      <tr>
        <td>8回</td>
        <td>25,000円<br>(3,125円/回)</td>
        <td>4ヵ月</td>
      </tr>
      <tr>
        <td>4回</td>
        <td>13,000円<br>(3,250円/回)</td>
        <td>2ヵ月</td>
      </tr>
      <tr>
        <td>レンタルマット</td>
        <td>300円/回</td>
        <td>当日</td>
      </tr>
    </table>
    <p>お支払方法</p>
    <p>現金</p>
  </div>
  <div class="trail-box01">
    <h2>お昼寝ヨガ ＆ 笑い</h2>
    <p class="mb20">「お昼寝ヨガ」と「笑い」を楽しみたい方のためのプランです。<br>ゆったりとした時間のなかで身体と心を休めてください。</p>
    <table class="trail-table-s01">
      <tr>
        <th></th>
        <th>料金</th>
        <th>有効期限</th>
      </tr>
      <tr>
        <td>5回チケット</td>
        <td>5,000円</td>
        <td>1ヵ月</td>
      </tr>
      <tr>
        <td>レンタルマット</td>
        <td>無料</td>
        <td>-</td>
      </tr>
    </table>
    <p>お支払い方法</p>
    <p>クレジットカード　VISA ／ Master ／ JCBまたは現金</p>
    <p>※回数券は都度スタジオにてご購入いただけます。</p>
  </div>
  <div class="trail-box01">
    <h2>体験レッスン</h2>
    <p class="mb20">スケジュールの中から参加希望のクラスを自由にお選びいただけます。<br>事前予約不要。レッスン開始時間の10分前までにスタジオにお越しください。
    <a href="<?php echo home_url('/register/'); ?>">詳しくは、体験レッスンのページへ。</a></p>
    <table class="trail-table-s01">
      <tr>
        <th></th>
        <th>料金</th>
        <th>有効期限</th>
      </tr>
      <tr>
        <td>1回体験<br>チケット</td>
        <td>1,500円</td>
        <td>当日</td>
      </tr>
      <tr>
        <td>2回体験<br>チケット</td>
        <td>2,000円<br>(1,000円/回)</td>
        <td>1週間</td>
      </tr>
      <tr>
        <td>3回体験<br>チケット</td>
        <td>2,500円<br>(840円/回)</td>
        <td>1週間</td>
      </tr>
      <tr>
        <td>レンタルマット</td>
        <td>無料</td>
        <td>-</td>
      </tr>
    </table>
    <p>お支払い方法</p>
    <p>現金</p>
    <p>※回数券は都度スタジオにてご購入いただけます。</p>
  </div>
  <div class="trail-box01">
    <h2>プライベートレッスン・出張レッスン</h2>
    <p class="mb20">マナヨガでは、プライベートレッスン、お友達やサークルなど仲間限定レッスン、ご家庭や企業への<br> 出張レッスンも行っております。<br>ご希望の日時、場所、 内容などご要望がござい ましたらお問い合わせください。<br>
    <a href="mailto:info@manayoga.jp">info@manayoga.jp</a></p>
    <h3>プライベートレッスン</h3>
    <table class="trail-table-s02">
      <tr>
        <th>人数</th>
        <th>1時間あたりの料金</th>
      </tr>
      <tr>
        <td>1名</td>
        <td>10,000円</td>
      </tr>
      <tr>
        <td>2・3名</td>
        <td>10,000円<br>(約3,300～5,000円/人)</td>
      </tr>
      <tr>
        <td>4～8名</td>
        <td>20,000円<br>(2,500～5,000円/人)</td>
      </tr>
    </table>
    <p class="mb50">キャンセルの際は、レッスン開始日の3日前までにご連絡ください。<br>それ以降のキャンセルとなる場合、また無断でのキャンセルの場合は、<br>キャンセル料として50%をご請求いたします。</p>
    <p>お支払い方法</p>
    <p>現金</p>
  </div>
  <div class="trail-box01">
    <h3>出張レッスン</h3>
    <table class="trail-table-s02">
      <tr>
        <th>人数</th>
        <th>1レッスンあたりの料金</th>
      </tr>
      <tr>
        <td>要相談</td>
        <td>10,000円～</td>
      </tr>
    </table>
    <p class="mb50">キャンセルの際は、レッスン開催日の前日の21:00までにご連絡ください。それ以降のキャンセルとなる場合、また無断でのキャンセルの場合は、キャンセル料としてレッスン料の50％をご請求いたします。</p>
    <p>お支払い方法</p>
    <p>現金</p>
  </div>
</section>

<?php get_footer(); ?>
