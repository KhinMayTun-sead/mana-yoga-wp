<?php get_header(); ?>

    <div class="banner mb40">
      <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor_bnr.jpg" alt="">
    </div><!-- banner -->

    <section class="list inner">
      <ul class="cf">
        <li><a href="<?php echo home_url('/instructor/instructor01/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img01.png" alt="IKUMI"></a></li>
<!--				    <li><a href="<?php echo home_url('/instructor/instructor02/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/instructor02.png" alt="TAKAKO"></a></li>-->
        <li><a href="<?php echo home_url('/instructor/instructor03/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img03.png" alt="福井れい"></a></li>
<!--       <li><a href="<?php echo home_url('/instructor/instructor04/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img04.png" alt="Minori"></a></li>-->
        <!--<li><a href="<?php echo home_url('/instructor/instructor05/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img05.png" alt="megumi"></a></li>-->
        <li><a href="<?php echo home_url('/instructor/instructor06/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img06.png" alt="エリコ"></a></li>
        <li><a href="<?php echo home_url('/instructor/instructor07/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img07.png" alt="Hitomi"></a></li>
<!--				    <li><a href="<?php echo home_url('/instructor/instructor08/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img08.png" alt="toco"></a></li>-->
<!--        <li><a href="<?php echo home_url('/instructor/instructor09/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img09.png" alt="HARU"></a></li>-->
<!--				    <li><a href="<?php echo home_url('/instructor/instructor10/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img10.png" alt="ひろこ"></a></li>-->
        <li><a href="<?php echo home_url('/instructor/instructor11/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img11.png" alt="しん"></a></li>
<!--        <li><a href="<?php echo home_url('/instructor/instructor12/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img12.png" alt="大橋真喜子"></a></li>-->
<!--        <li><a href="<?php echo home_url('/instructor/instructor14/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img14.png" alt="emi"></a></li>-->
        <li><a href="<?php echo home_url('/instructor/instructor15/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img15.png" alt="kasumi"></a></li>
<!--				    <li><a href="<?php echo home_url('/instructor/instructor16/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img16.png" alt="hitomi"></a></li>-->
        <li><a href="<?php echo home_url('/instructor/instructor17/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img17.png" alt="ちひろ"></a></li>
        <!--<li><a href="<?php echo home_url('/instructor/instructor18/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img18.png" alt="Kazumi"></a></li>-->
<!--				    <li><a href="<?php echo home_url('/instructor/instructor19/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img19.png" alt="サトミ"></a></li>-->
<!--        <li><a href="<?php echo home_url('/instructor/instructor21/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img21.png" alt="MASA"></a></li>-->
        <li><a href="<?php echo home_url('/instructor/instructor23/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img23.png" alt="りょうこ"></a></li>
      </ul>
    </section>

<?php get_footer(); ?>
