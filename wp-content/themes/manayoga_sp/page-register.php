<?php get_header(); ?>

<div class="banner mb40">
  <img src="<?php echo get_template_directory_uri(); ?>/img/register/register_bnr_img.jpg" alt="">
</div>

<section class="trail-lesson inner">
  <div class="trail-box01">
    <h2>体験レッスンに申し込む</h2>
    <p><a href="<?php echo home_url('/schedule/'); ?>">レッスンスケジュール</a>を確認し、<a href="https://coubic.com/manayogastudio/services" target="_blank">専用の予約システム</a>より受けたいレッスンを選び、予約してください。<br>予約完了メールが届きます。<br>届かない場合は<a href="mailto:info@manayoga.jp">info@manayoga.jp</a>までご連絡ください。</p>
  </div>
  <div class="trail-box01">
    <h2>レッスン当日</h2>
    <p class="mb70">レッスン開始10分前までにスタジオにお越しください。<br>場所は<a href="https://www.google.co.jp/maps/place/%E3%83%9E%E3%83%8A%E3%83%A8%E3%82%AC%E3%82%B9%E3%83%80%E3%82%B8%E3%82%AA/@35.6484834,139.7065481,17z/data=!3m1!4b1!4m2!3m1!1s0x60188b4176d91c3f:0xbb4d894d54ad8bb6?hl=ja" target="_blank">渋谷区恵比寿西1-2-1 エビスマンション706</a>。恵比寿駅西口から徒歩2分です。<br><a href="../access/">アクセスページへ</a></p>
    <p>着替えやお荷物の保管については、インストラクターがご案内します。</p>
  </div>
  <div class="trail-box01">
    <h2>体験レッスン時の服装について</h2>
    <ul>
      <li>Tシャツ、タンクトップ、ジャージ、スパッツ、ハーフパンツなど、動きやすい格好であれば大丈夫です。</li>
      <li>レッスンは裸足で行いますので、靴は不要です。</li>
      <li>ヨガマットはスタジオで無料でお貸出しいたします。</li>
      <li>ウェアの無料貸出しも行っております。</li>
    </ul>
  </div>
  <div class="trail-box01">
    <h2>体験レッスンの料金</h2>
    <table class="trail-table-s01">
      <tr>
        <th></th>
        <th>料金</th>
        <th>有効期限</th>
      </tr>
      <tr>
        <td>1回体験<br>チケット</td>
        <td>1,500円</td>
        <td>当日</td>
      </tr>
      <tr>
        <td>2回体験<br>チケット</td>
        <td>2,000円<br>(1,000円/回)</td>
        <td>1週間</td>
      </tr>
      <tr>
        <td>3回体験<br>チケット</td>
        <td>2,500円<br>(840円/回)</td>
        <td>1週間</td>
      </tr>
      <tr>
        <td>レンタルマット</td>
        <td>無料</td>
        <td>-</td>
      </tr>
    </table>
    <p>お支払い方法<br>現金<br>由の如何に関わらず、一度入金されたレッスン料は返金致しかねます。<br>またチケットは譲渡、転売はできかねます。</p>
  </div>
  <div class="trail-box01">
    <h2>おすすめクラス</h2>
    <p class="mb20">マナヨガの体験レッスンは、全てのクラスの中からお選び頂けます。<br>ヨガ未経験の方も、他にはないクラスをお探しの方も、気になるクラスをお気軽にお受けください。</p>
    <div class="recommen-class">
      <div class="recommen-inr cf">
        <div class="recommenL fl-l heightLine_01">
          <h3>ビギナー</h3>
        </div>
        <div class="recommenR fl-r heightLine_01">
          <p>ヨガは初めて、という方大歓迎！基礎から学べる、入門・初級クラスです。</p>
        </div>
      </div>
    </div>
    <div class="recommen-class">
      <div class="recommen-inr cf">
        <div class="recommenL fl-l heightLine_02">
          <h3>優しいヨガ</h3>
        </div>
        <div class="recommenR fl-r heightLine_02">
          <p>ひとつひとつのポーズを ゆっくり丁寧にお伝えしていきます。<br>初心者の方も安心の優しいレッスンです。</p>
        </div>
      </div>
    </div>
    <div class="recommen-class">
      <div class="recommen-inr cf">
        <div class="recommenL fl-l heightLine_03">
          <h3>お疲れ リリースヨガ</h3>
        </div>
        <div class="recommenR fl-r heightLine_03">
          <p>ゆったり心地よく身体を 伸ばしたりねじったりしながら、血流やリンパを刺激して 疲労物質を流していきます。</p>
        </div>
      </div>
    </div>
    <div class="recommen-class">
      <div class="recommen-inr cf">
        <div class="recommenL fl-l heightLine_04">
          <h3>お腹スッキリ ヨガ</h3>
        </div>
        <div class="recommenR fl-r heightLine_04">
          <p>体幹を強くして ポッコリお腹を解消、 内臓にも働きかけて デトックス効果を高めます。</p>
        </div>
      </div>
    </div>
    <div class="recommen-class">
      <div class="recommen-inr cf">
        <div class="recommenL fl-l heightLine_05">
          <h3>パワー</h3>
        </div>
        <div class="recommenR fl-r heightLine_05">
          <p>運動量が多く、ダイナミック、ハードな動きを呼吸に合わせて行います。 身体を動かしたい！ という方にはピッタリです。</p>
        </div>
      </div>
    </div>
    <div class="recommen-class">
      <div class="recommen-inr cf">
        <div class="recommenL fl-l heightLine_06">
          <h3>For Men</h3>
        </div>
        <div class="recommenR fl-r heightLine_06">
          <p>ヨガに興味はあるけど…そんな男性のための男性限定クラスです。<br>しっかり動いて締まった身体を目指します。</p>
        </div>
      </div>
    </div>
  </div>
  <div class="trail-box01">
    <h2>ご入会の手続き</h2>
    <p>体験レッスン終了後、ご入会についての説明をさせていただきます。<br>ご質問がございましたら、何でもおたずねください。<br>体験レッスンの最終日にご入会いただくと、入会金が無料になる特典もございます。</p>
  </div>
</section>

<?php get_footer(); ?>
