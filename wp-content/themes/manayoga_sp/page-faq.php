<?php get_header(); ?>

<div class="banner mb30">
  <img src="<?php echo get_template_directory_uri(); ?>/img/faq/faq_bnr.jpg" alt="yoga">
</div>

<main class="inner">
  <div class="faq-main mb40">
    <section class="faq-sub">
      <h3>体験レッスンについて</h3>
      <dl class="mb50">
        <dt class="mb20"><p>体験レッスンに申し込むにはどうすればいいですか？</p></dt>
        <dd class="mb40"><p><a href="<?php echo home_url('/schedule/'); ?>" class="txt-blue">スケジュール</a>を確認し、お好きなクラスを専用の予約システムより<br>ご予約ください。<br>詳しくはこちらのページもご覧ください。
        <span><a href="<?php echo home_url('/register/'); ?>" class="txt-blue">→体験レッスンについて</a></span>
        </p>
        </dd>
        <dt class="mb20"><p>ウェアはレンタルできますか？</p></dt>
        <dd class="mb40"><p>はい。マナヨガではレンタルウェアを無料でご用意しております。</p>
        </dd>
        <dt class="mb20"><p>着替えは必要ですか？</p></dt>
        <dd><p>動きやすい服装に着替えてください。更衣室・ロッカーもご用意しております。またレンタルウェアを無料でご用意しております。</p>
        </dd>
        <dt class="mb20"><p>体験レッスンは何回でも受けられますか？</p></dt>
        <dd class="mb40"><p>最大3回まで体験レッスンを受けていただけるチケットを販売しています。ご都合に合わせてお求めください。</p>
        </dd>
        <dt class="mb20"><p>服装はどんな格好がいいですか？</p></dt>
        <dd class="mb40"><p>ジャージやTシャツなど動きやすい格好でお願いします。体験レッスンは裸足で行いますので、ヨガ用の靴や靴下は不要です。</p>
        </dd>
        <dt class="mb20"><p>レッスン前に食事をしても大丈夫？</p></dt>
        <dd class="mb40"><p>軽く食べる程度でしたら問題ありません。運動の多いクラスもありますので、気分が悪くならないようご調節ください。</p>
        </dd>
      </dl>
    </section>
    <section class="faq-sub">
      <h3>予約・キャンセルについて</h3>
      <dl class="mb50">
        <dt class="mb20"><p>予約はどのように取ればいいのですか？</p></dt>
        <dd class="mb40"><p>スケジュールをご確認いただき、専用の予約フォームからご予約ください。<span><a href="<?php echo home_url('/schedule/'); ?>" class="txt-blue">→スケジュールへ</a></span></p>
        </dd>
        <dt class="mb20"><p>予約をしなくてもレッスンが受けられますか？</p></dt>
        <dd class="mb40"><p>事前予約が必要となります。レッスン開始予定の3時間前までに専用の予約システムよりご予約ください。</p>
        </dd>
        <dt><p>キャンセルしたいのですが、どのようにすればいいですか？</p></dt>
        <dd class="mb40"><p>あらかじめお電話またはメールにてご連絡ください。プライベートレッスン・出張レッスンはキャンセル料が発生いたします。<span><a href="<?php echo home_url('/price/'); ?>" class="txt-blue">→料金のご案内を見る</a></span></p>
        </dd>
      </dl>
    </section>
    <section class="faq-sub">
      <h3>料金について</h3>
      <dl class="mb50">
        <dt class="mb20"><p>マンスリー会員でも笑いヨガを受けられますか？</p></dt>
        <dd class="mb40"><p>はい、受けられます。</p></dd>
        <dt><p>マンスリー2・4・8回の会員です。今月もう1回行きたいのですが・・・</p></dt>
        <dd class="mb40"><p>1回あたり2,500円で追加レッスンをお受けいただけます。</p></dd>
        <dt><p>マンスリー8回の会員です。今月6回しか行けなかったのですが・・・</p></dt>
        <dd class="mb40"><p>月内に使用できなかったレッスンの1回分は翌月に持ち越すことは可能です。レッスン回数の変更などインストラクターにご相談ください。</p></dd>
        <dt><p>私のカードで友だちもレッスンを受けることができますか？</p></dt>
        <dd class="mb40"><p>できません。まずは体験レッスンへご参加いただくか、ご入会ください。
        <span><a href="<?php echo home_url('/register/'); ?>" class="txt-blue">→体験レッスンについて</a></span>
        </p></dd>
        <dt class="mb20"><p>マンスリー会員になるのはなにが必要ですか？</p></dt>
        <dd class="mb40"><p>クレジットカードが必要です。</p></dd>
        <dt class="mb20"><p>マンスリー会員を解約するにはどうすればいいですか？</p></dt>
        <dd class="mb40"><p>退会希望月の前々月の決済予定日前日までにお申し出ください。</p></dd>
      </dl>
    </section>
    <section class="faq-sub">
      <h3>スタジオの利用について</h3>
      <dl class="mb50">
        <dt class="mb20"><p>定休日はありますか？</p></dt>
        <dd class="mb40"><p>定休日はありません。年末年始やお盆の時期にお休みをいただいています。その他、インストラクターの研修などで臨時でお休みをいただく場合は、事前に告知いたします。</p></dd>
        <dt class="mb20"><p>会員カードを失くしてしまいました・・・</p></dt>
        <dd class="mb40"><p>インストラクターまでご連絡ください。カード再発行の手続きを行います。</p></dd>
        <dt class="mb20"><p>トイレや更衣室はありますか？</p></dt>
        <dd class="mb40"><p>ご用意しております。</p></dd>
        <dt class="mb20"><p>駐車場はありますか？</p></dt>
        <dd class="mb40"><p>近隣にコインパーキングがございます。そちらをご利用ください。</p></dd>
        <dt class="mb20"><p>ビルのエントランスが開かないのですが・・・</p></dt>
        <dd class="mb40"><p>20：00以降エントランスが施錠されます。遅い時間のレッスンにご予約された場合は、事前にパスワードをお渡しいたします。</p></dd>
      </dl>
    </section>
  </div>
</main>

<section class="instructor inner">
  <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01_img10.png">
  <div class="autoplay">
    <?php instructor_list(); ?>
  </div>
</section><!-- instructor-->

<?php get_footer(); ?>
