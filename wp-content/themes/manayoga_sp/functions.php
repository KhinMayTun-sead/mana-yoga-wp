<?php
function catbase_function($link) {
    return str_replace("/category/", "/", $link);
}
function catbase_flush_rules() {
    global $wp_rewrite;
    $wp_rewrite->flush_rules();
}
function catbase_rewrite($wp_rewrite) {
    $new_rules = array('(.+)/page/(.+)/?' => 'index.php?category_name='.$wp_rewrite->preg_index(1).'&paged='.$wp_rewrite->preg_index(2));
    $wp_rewrite->rules = $new_rules + $wp_rewrite->rules;
}
add_filter('user_trailingslashit', 'catbase_function');
add_action('init', 'catbase_flush_rules');
add_filter('generate_rewrite_rules', 'catbase_rewrite');

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// インストラクター
function instructor_list()
{
  ?>
    <div><a href="<?php echo home_url('/instructor/instructor01/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img01.png"></a></div>
    <div><a href="<?php echo home_url('/instructor/instructor07/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img07.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor02/'); ?>"><img src="../common/images/instructor_img02.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor03/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img03.png"></a></div>
    <div><a href="<?php echo home_url('/instructor/instructor04/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img04.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor05/'); ?>"><img src="../common/images/instructor_img05.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor06/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img06.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor08/'); ?>"><img src="../common/images/instructor_img08.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor09/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img09.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor10/'); ?>"><img src="../common/images/instructor_img10.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor11/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img11.png"></a></div>
    <div><a href="<?php echo home_url('/instructor/instructor12/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img12.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor13/'); ?>"><img src="../common/images/instructor_img13.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor14/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img14.png"></a></div>
    <div><a href="<?php echo home_url('/instructor/instructor15/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img15.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor16/'); ?>"><img src="../common/images/instructor_img16.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor17/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img17.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor18/'); ?>"><img src="../common/images/instructor_img18.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor19/'); ?>"><img src="../common/images/instructor_img19.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor20/'); ?>"><img src="../common/images/instructor_img20.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor21/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img21.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor22/'); ?>"><img src="../common/images/instructor_img22.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor23/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img23.png"></a></div>
    <?php
}
