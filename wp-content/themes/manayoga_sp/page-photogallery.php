<?php get_header(); ?>

<div class="banner mb30">
  <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/gallery_bnr.jpg" alt="yoga">
</div>
<main class="inner mb40">
  <div class="photo-sub mb40">
    <section>
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery01.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery02.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery03.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery04.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery05.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery06.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery07.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery08.png" alt="" class="mb20">
      <img src="<?php echo get_template_directory_uri(); ?>/img/photogallery/photogallery09.png" alt="" id="aumnie" class="mb40">
    </section>
  </div>

  <section class="instructor inner">
    <img src="<?php echo get_template_directory_uri(); ?>/img/instructor/instructor01_img10.png">
    <div class="autoplay">
      <?php instructor_list(); ?>
    </div>
  </section><!-- instructor-->

  <?php get_footer(); ?>
