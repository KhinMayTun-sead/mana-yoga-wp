<?php get_header(); ?>
<div class="banner mb30">
  <img src="<?php echo get_template_directory_uri(); ?>/common/images/top_bnr.jpg" alt="yoga">
</div>

<main>
  <section class="advert">
    <div class="descript">
      <p>マナヨガは恵比寿にある少人数制のヨガスタジオです。<br>リラックスやダイエットなど、ひとりひとりの目的に合わせてレッスンを行っています。<br>体が硬い、運動が苦手という方もご安心ください。<br>ティーチャートレーニングを修了した<br>インストラクターが、しっかり向き合います。<br>自分自身のカラダと心と対話しながら、<br>私たちと一緒にヨガで心地よいひとときを過ごしませんか。</p>
    </div>
    <div class="info inner">
      <div class="brown"><img src="<?php echo get_template_directory_uri(); ?>/common/images/info.jpg" alt="info"></div>
      <div class="lett mb50">
        <p>マナヨガは、2015年5月にオープンした恵比寿にあるヨガスタジオです。<br>ヨガにご興味がある方は、ぜひお気軽にお越しください。</p>
      </div>
    </div>
    <div class="yoga-blog inner mb30">
      <div class="yoga-blog-head">
        <h2><a href="<?php echo home_url('/blog/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/yoga_blog.jpg" alt="blog"></a></h2>
      </div>
      <div class="yoga-blog-content cf">
        <ul>
            <?php
            $newslist = get_posts( array(
            'posts_per_page' => 5, //取得記事件数
            'cat'            =>-2
            ));
            foreach( $newslist as $post ):
            setup_postdata( $post );
            ?>

            <li><a href="<?php the_permalink(); ?>"><date><?php echo get_the_date('Y年m月d日') ?></date><span><?php the_title(); ?></span></a></li>
            <?php
            endforeach;
            wp_reset_postdata();
            ?>

        </ul>
        <a href="<?php echo home_url('/blog/'); ?>" class="fl-r blog-view-list">一覧を見る</a>
      </div>
    </div>
    <div class="first mb40">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/first_bg.jpg" alt="">
    </div>
    <div class="feature mb25">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/yoga_feat.jpg" alt="feature">
    </div>
    <div class="qualifi inner">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/yoga_qualifi.jpg" alt="qualifi">
    </div>
    <div class="lett-01 same inner">
      <p>インストラクター全員が、ヨガをきちんと学んだ証である全米ヨガアライアンスの資格“RYT200”を保持しています。さらにそれぞれが異なったバックグラウンドを持つ個性あふれる私たちがみなさんのヨガライフをステキに彩るお手伝いをいたします。</p>
    </div>
    <div class="yoga-care inner">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/yoga_care.png" alt="care">
    </div>
    <div class="lett-02 same inner">
      <p>みなさんのことをすぐに覚えられるように、少しの変化にも気づけるように、みなさんの声に耳を傾けられるように…<br>私たちの「少しでも寄り添ってヨガをお伝えできたら」という気持ちから、クラスは最大8名の少人数にしました。</p>
    </div>
    <div class="safety inner">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/yoga_safety.jpg" alt="safety">
    </div>
    <div class="lett-03 same inner">
      <p>インストラクター全員が、ヨガをきインストラクター全員が、みなさんの体調や近況など、把握しておくべきことを共有するためにおひとりずつのカルテを作成します。みなさんに安心してレッスンを受けていただけるよう、細心の注意を払い、体の安全をサポートします。</p>
    </div>
    <div class="intro">
      <img src="<?php echo get_template_directory_uri(); ?>/common/images/intro.jpg" alt="introduction">
    </div>
  </section>
  <section class="instructor inner">
  <div class="autoplay">
    <div><a href="<?php echo home_url('/instructor/instructor01/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img01.png"></a></div>
    <div><a href="<?php echo home_url('/instructor/instructor07/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img07.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor02/'); ?>"><img src="../common/images/instructor_img02.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor03/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img03.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor04/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img04.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor05/'); ?>"><img src="../common/images/instructor_img05.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor06/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img06.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor08/'); ?>"><img src="../common/images/instructor_img08.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor09/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img09.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor10/'); ?>"><img src="../common/images/instructor_img10.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor11/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img11.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor12/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img12.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor13/'); ?>"><img src="../common/images/instructor_img13.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor14/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img14.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor15/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img15.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor16/'); ?>"><img src="../common/images/instructor_img16.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor17/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img17.png"></a></div>
    <!-- <div><a href="<?php echo home_url('/instructor/instructor18/'); ?>"><img src="../common/images/instructor_img18.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor19/'); ?>"><img src="../common/images/instructor_img19.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor20/'); ?>"><img src="../common/images/instructor_img20.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor21/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img21.png"></a></div> -->
    <!-- <div><a href="<?php echo home_url('/instructor/instructor22/'); ?>"><img src="../common/images/instructor_img22.png"></a></div> -->
    <div><a href="<?php echo home_url('/instructor/instructor23/'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/common/images/instructor_img23.png"></a></div>
  </div>
  </section><!-- instructor-->
  </div>
  <?php get_footer(); ?>
