<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'clinic-matsumoto_mnyg');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'clinic-matsumoto');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', '123');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4ZJ#T`M5T*-^P.(%Z{)XI!+DB?J-%}AM!2esMZSi#@gXC -U4Viu|b`WBC!;mA*]');
define('SECURE_AUTH_KEY',  '|ARwe;u3hh+`LU|K#B7nA3~va&NFkKU~]l{Fa+{E^/93g8d:x%{iE:!fZ8(]PK|x');
define('LOGGED_IN_KEY',    '!ZF$nf&jH}|<GzD:+DLz[U41_|qA(V,&zh4R?<u(9g2wm!q%[$n+#;)2{v1hK3P*');
define('NONCE_KEY',        'k( a;e3aEz;Yg-{A GvEg-4!B-R`TVb})~$eJAW|r{D;{B`;9.IMDeGL?G)&&jK-');
define('AUTH_SALT',        ':nWev;<{{q+) 3>H|6N 9KIx?Qx@mX1AqsUKGo!v[F|>NgI=2u(Z/DrShxl#-}(]');
define('SECURE_AUTH_SALT', 'uSeZ^(9lXTUSvZ;Xa& +9vm)u*8;-*nI&9z>^$ v79|]#dB`8+Vdy.-bw!cm|LWP');
define('LOGGED_IN_SALT',   '#WZLf/!DNsL3^,#p(Lc(,k#>_0CW RBlhuJ]XcJX=U_0(--[a9q,(XEubt/)mCX(');
define('NONCE_SALT',       'K;QP~|yh_s4Vx%Tbsxw`hkGkF+~ cxRp!EF4~$#(6N0fL&v.wgZYu#3t~?2b2UJk');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'mnyg_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
